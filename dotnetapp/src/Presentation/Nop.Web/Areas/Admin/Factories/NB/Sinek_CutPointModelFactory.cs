﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.NB;
using Nop.Services.Catalog;
using Nop.Services.Localization;
using Nop.Services.NB;
using Nop.Web.Areas.Admin.Infrastructure.Mapper.Extensions;
using Nop.Web.Areas.Admin.Models.NB;
using Nop.Web.Framework.Factories;
using Nop.Web.Framework.Models.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nop.Web.Areas.Admin.Factories.NB
{
	/// <summary>
	/// Represents the sinekcutpoint model factory implementation
	/// </summary>
	public class Sinek_CutPointModelFactory : ISinek_CutPointModelFactory
	{
		#region Fields

		private readonly ISinek_ModelService _sinek_ModelService;
		private readonly ILocalizationService _localizationService;
		private readonly CatalogSettings _catalogSettings;
		private readonly ILocalizedModelFactory _localizedModelFactory;
		private readonly IManufacturerService _manufacturerService;
		private readonly IBaseAdminModelFactory _baseAdminModelFactory;
		private readonly ISinek_CutPointService _sinek_CutPointService;

		#endregion

		#region Ctor

		public Sinek_CutPointModelFactory(ISinek_ModelService sinek_ModelService,
			ILocalizationService localizationService,
			CatalogSettings catalogSettings,
			ILocalizedModelFactory localizedModelFactory,
			IManufacturerService manufacturerService,
			IBaseAdminModelFactory baseAdminModelFactory,
			ISinek_CutPointService sinek_CutPointService)
		{
			_sinek_ModelService = sinek_ModelService;
			_localizationService = localizationService;
			_catalogSettings = catalogSettings;
			_localizedModelFactory = localizedModelFactory;
			_manufacturerService = manufacturerService;
			_baseAdminModelFactory = baseAdminModelFactory;
			_sinek_CutPointService = sinek_CutPointService;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Prepare sinekCutpoint search model
		/// </summary>
		/// <param name="searchModel">Sinek_CutPoint_View search model</param>
		/// <returns>Sinek_CutPoint_View search model</returns>
		public virtual Sinek_CutPoint_ViewSearchModel PrepareSinekCutpointViewSearchModel(Sinek_CutPoint_ViewSearchModel searchModel)
		{
			if (searchModel == null)
				throw new ArgumentNullException(nameof(searchModel));

			//prepare available manufactures
			_baseAdminModelFactory.PrepareManufacturers(searchModel.AvailableManufacturers);

			//prepare page parameters
			searchModel.SetGridPageSize();

			return searchModel;
		}

		/// <summary>
		/// Prepare paged sinekCutpointmodelview list model
		/// </summary>
		/// <param name="searchModel">sinekCutpointmodelView search model</param>
		/// <returns>sinekCutpointView list model</returns>
		public virtual Sinek_CutPoint_ViewListModel PrepareSinekCutpointViewListModel(Sinek_CutPoint_ViewSearchModel searchModel)
		{
			if (searchModel == null)
				throw new ArgumentNullException(nameof(searchModel));

			//get records
			var sinekCutPointsViews = _sinek_CutPointService.GetAllSinek_CutPoints_View(searchModel.ModelName, searchModel.ManufacturerId,
								   pageIndex: searchModel.Page - 1, pageSize: searchModel.PageSize, showHidden: true);

			//prepare grid model
			var model = new Sinek_CutPoint_ViewListModel().PrepareToGrid(searchModel, sinekCutPointsViews, () =>
			{
				return sinekCutPointsViews.Select(sm =>
				{
					//fill in model values from the entity
					var sinekCutpointViewModel = sm.ToModel<Sinek_CutPoint_ViewModel>();

					return sinekCutpointViewModel;
				});
			});

			return model;
		}

		/// <summary>
		/// Prepare sinekCutpoint model
		/// </summary>
		/// <param name="model">sinekCutpointmodel model</param>
		/// <param name="sinekCutpoint">sinekCutpointmodel</param>
		/// <param name="excludeProperties">Whether to exclude populating of some properties of model</param>
		/// <returns>Sinek_CutPoint model</returns>
		public virtual Sinek_CutPointModel PrepareSinekCutpointModel(Sinek_CutPointModel model, Sinek_CutPoint sinekCutpoint, bool excludeProperties = false)
		{
			Action<Sinek_CutPointLocalizedModel, int> localizedModelConfiguration = null;

			if (sinekCutpoint != null)
			{
				//fill in model values from the entity
				if (model == null)
				{
					model = sinekCutpoint.ToModel<Sinek_CutPointModel>();
				}

				//define localized model configuration action
				localizedModelConfiguration = (locale, languageId) =>
				{
					locale.ManufacturerId = _localizationService.GetLocalized(sinekCutpoint, entity => entity.ManufacturerId, languageId, false, false);
					locale.ModelId = _localizationService.GetLocalized(sinekCutpoint, entity => entity.ModelId, languageId, false, false);
					locale.Size = _localizationService.GetLocalized(sinekCutpoint, entity => entity.Size, languageId, false, false);
					locale.CutPoint = _localizationService.GetLocalized(sinekCutpoint, entity => entity.CutPoint, languageId, false, false);
					locale.DisplayOrder = _localizationService.GetLocalized(sinekCutpoint, entity => entity.DisplayOrder, languageId, false, false);
				};
			}

			//set default values for the new model
			if (sinekCutpoint == null)
			{
				model.PageSize = 4;
				model.PageSizeOptions = _catalogSettings.DefaultSinek_CutPointPageSizeOptions;
				model.Published = true;
			}

			//prepare localized models
			if (!excludeProperties)
				model.Locales = _localizedModelFactory.PrepareLocalizedModels(localizedModelConfiguration);

			//prepare available manufactures
			_baseAdminModelFactory.PrepareManufacturers(model.AvailableManufacturerId,
				defaultItemText: "Select Blade Make");

			//prepare available sinekmodels
			var sinekModels = _sinek_ModelService.GetSinek_ModelByManufacturerId(model.ManufacturerId).ToList();
			foreach (var s in sinekModels)
			{
				model.AvailableModelId.Add(new SelectListItem() { Text = s.Name, Value = s.Id.ToString(), Selected = (s.Id == model.ModelId) });
			}

			return model;
		}

		#endregion
	}
}
