﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.NB;
using Nop.Services.Catalog;
using Nop.Services.Localization;
using Nop.Services.NB;
using Nop.Web.Areas.Admin.Infrastructure.Mapper.Extensions;
using Nop.Web.Areas.Admin.Models.NB;
using Nop.Web.Framework.Factories;
using Nop.Web.Framework.Models.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nop.Web.Areas.Admin.Factories.NB
{
	/// <summary>
	/// Represents the sinekcutlocation model factory implementation
	/// </summary>
	public class Sinek_LocationModelFactory : ISinek_LocationModelFactory
	{
		#region Fields

		private readonly ISinek_ModelService _sinek_ModelService;
		private readonly ILocalizationService _localizationService;
		private readonly CatalogSettings _catalogSettings;
		private readonly ILocalizedModelFactory _localizedModelFactory;
		private readonly IManufacturerService _manufacturerService;
		private readonly IBaseAdminModelFactory _baseAdminModelFactory;
		private readonly ISinek_CutPointService _sinek_CutPointService;
		private readonly ISinek_LocationService _sinek_LocationService;

		#endregion

		#region Ctor

		public Sinek_LocationModelFactory(ISinek_ModelService sinek_ModelService,
			ILocalizationService localizationService,
			CatalogSettings catalogSettings,
			ILocalizedModelFactory localizedModelFactory,
			IManufacturerService manufacturerService,
			IBaseAdminModelFactory baseAdminModelFactory,
			ISinek_CutPointService sinek_CutPointService,
			ISinek_LocationService sinek_LocationService)
		{
			_sinek_ModelService = sinek_ModelService;
			_localizationService = localizationService;
			_catalogSettings = catalogSettings;
			_localizedModelFactory = localizedModelFactory;
			_manufacturerService = manufacturerService;
			_baseAdminModelFactory = baseAdminModelFactory;
			_sinek_CutPointService = sinek_CutPointService;
			_sinek_LocationService = sinek_LocationService;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Prepare sinekLocation search model
		/// </summary>
		/// <param name="searchModel">Sinek_Location search model</param>
		/// <returns>Sinek_Location search model</returns>
		public virtual Sinek_LocationSearchModel PrepareSinekLocationSearchModel(Sinek_LocationSearchModel searchModel)
		{
			if (searchModel == null)
				throw new ArgumentNullException(nameof(searchModel));

			//prepare page parameters
			searchModel.SetGridPageSize();

			return searchModel;
		}

		/// <summary>
		/// Prepare paged sinekLocation list model
		/// </summary>
		/// <param name="searchModel">sinekLocation search model</param>
		/// <returns>sinekLocation list model</returns>
		public virtual Sinek_LocationListModel PrepareSinekLocationListModel(Sinek_LocationSearchModel searchModel)
		{
			if (searchModel == null)
				throw new ArgumentNullException(nameof(searchModel));

			//get records
			var sinekLocations = _sinek_LocationService.GetAllSinek_Locations(searchModel.Name1, searchModel.Name2,
								   pageIndex: searchModel.Page - 1, pageSize: searchModel.PageSize, showHidden: true);

			//prepare grid model
			var model = new Sinek_LocationListModel().PrepareToGrid(searchModel, sinekLocations, () =>
			{
				return sinekLocations.Select(sm =>
				{
					//fill in model values from the entity
					var sinekLocationModel = sm.ToModel<Sinek_LocationModel>();

					return sinekLocationModel;
				});
			});

			return model;
		}

		/// <summary>
		/// Prepare sinekLocation model
		/// </summary>
		/// <param name="model">sinekLocation model</param>
		/// <param name="sinekLocation">sinekLocation entity</param>
		/// <param name="excludeProperties">Whether to exclude populating of some properties of model</param>
		/// <returns>Sinek_Location model</returns>
		public virtual Sinek_LocationModel PrepareSinekLocationModel(Sinek_LocationModel model, Sinek_Location sinekLocation, bool excludeProperties = false)
		{
			Action<Sinek_LocationLocalizedModel, int> localizedModelConfiguration = null;

			if (sinekLocation != null)
			{
				//fill in model values from the entity
				if (model == null)
				{
					model = sinekLocation.ToModel<Sinek_LocationModel>();
				}

				//define localized model configuration action
				localizedModelConfiguration = (locale, languageId) =>
				{
					locale.EMail = _localizationService.GetLocalized(sinekLocation, entity => entity.EMail, languageId, false, false);
					locale.Fax = _localizationService.GetLocalized(sinekLocation, entity => entity.Fax, languageId, false, false);
					locale.LocationAddress = _localizationService.GetLocalized(sinekLocation, entity => entity.LocationAddress, languageId, false, false);
					locale.Name1 = _localizationService.GetLocalized(sinekLocation, entity => entity.Name1, languageId, false, false);
					locale.Name2= _localizationService.GetLocalized(sinekLocation, entity => entity.Name2, languageId, false, false);
					locale.Notes= _localizationService.GetLocalized(sinekLocation, entity => entity.Notes, languageId, false, false);
					locale.PhoneNo1= _localizationService.GetLocalized(sinekLocation, entity => entity.PhoneNo1, languageId, false, false);
					locale.PhoneNo2= _localizationService.GetLocalized(sinekLocation, entity => entity.PhoneNo2, languageId, false, false);
					locale.SellsRockerz= _localizationService.GetLocalized(sinekLocation, entity => entity.SellsRockerz, languageId, false, false);
					locale.SellsSk8tape= _localizationService.GetLocalized(sinekLocation, entity => entity.SellsSk8tape, languageId, false, false);
					locale.WebSite= _localizationService.GetLocalized(sinekLocation, entity => entity.WebSite, languageId, false, false);
				};
			}

			//set default values for the new model
			if (sinekLocation == null)
			{
				model.PageSize = 4;
				model.PageSizeOptions = _catalogSettings.DefaultSinek_LocationPageSizeOptions;
				model.Published = true;
			}

			//prepare localized models
			if (!excludeProperties)
				model.Locales = _localizedModelFactory.PrepareLocalizedModels(localizedModelConfiguration);

			return model;
		}

		#endregion
	}
}
