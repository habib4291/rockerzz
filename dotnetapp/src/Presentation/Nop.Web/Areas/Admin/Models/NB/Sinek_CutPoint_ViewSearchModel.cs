﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;
using System.Collections.Generic;

namespace Nop.Web.Areas.Admin.Models.NB
{
	/// <summary>
	/// Represents a sinek cutpoint view search model
	/// </summary>
	public partial class Sinek_CutPoint_ViewSearchModel : BaseSearchModel
    {
        #region Ctor

        public Sinek_CutPoint_ViewSearchModel()
        {
            AvailableManufacturers = new List<SelectListItem>();
        }

        #endregion

        #region Properties

        [NopResourceDisplayName("Admin.Catalog.SinekCutPoint.Search.Fields.ModelName")]
        public string ModelName { get; set; }

        [NopResourceDisplayName("Admin.Catalog.SinekCutPoint.Search.Fields.Manufacturer")]
        public int ManufacturerId { get; set; }
        public IList<SelectListItem> AvailableManufacturers { get; set; }

        #endregion
    }
}
