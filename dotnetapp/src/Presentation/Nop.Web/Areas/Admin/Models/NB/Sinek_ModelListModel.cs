﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.NB
{
	/// <summary>
	/// Represents a sinek model list model
	/// </summary>
	public partial class Sinek_ModelListModel : BasePagedListModel<Sinek_ModelModel>
    {
    }
}
