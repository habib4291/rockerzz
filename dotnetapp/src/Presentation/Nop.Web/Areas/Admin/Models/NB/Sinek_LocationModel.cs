﻿using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;
using System;
using System.Collections.Generic;

namespace Nop.Web.Areas.Admin.Models.NB
{
	/// <summary>
	/// Represents a Sinek_Location model
	/// </summary>
	public class Sinek_LocationModel : BaseNopEntityModel, ILocalizedModel<Sinek_LocationLocalizedModel>
	{
		#region Ctor

		public Sinek_LocationModel()
		{
			if (PageSize < 1)
			{
				PageSize = 5;
			}
			Locales = new List<Sinek_LocationLocalizedModel>();
		}

		#endregion

		#region Properties

		[NopResourceDisplayName("Admin.Catalog.Sinek_Locations.Fields.Latitude")]
		public double Latitude { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Locations.Fields.Longitude")]
		public double Longitude { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Locations.Fields.LocationType")]
		public string LocationType { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Locations.Fields.Name1")]
		public string Name1 { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Locations.Fields.Name2")]
		public string Name2 { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Locations.Fields.LocationAddress")]
		public string LocationAddress { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Locations.Fields.PhoneNo1")]
		public string PhoneNo1 { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Locations.Fields.PhoneNo2")]
		public string PhoneNo2 { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Locations.Fields.Fax")]
		public string Fax { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Locations.Fields.WebSite")]
		public string WebSite { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Locations.Fields.EMail")]
		public string EMail { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Locations.Fields.Notes")]
		public string Notes { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Locations.Fields.SellsSk8tape")]
		public bool SellsSk8tape { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Locations.Fields.SellsRockerz")]
		public bool SellsRockerz { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Locations.Fields.PageSize")]
		public int PageSize { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Locations.Fields.PageSizeOptions")]
		public string PageSizeOptions { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Locations.Fields.Published")]
		public bool Published { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Locations.Fields.Deleted")]
		public bool Deleted { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Locations.Fields.CreatedOnUtc")]
		public DateTime CreatedOnUtc { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Locations.Fields.UpdatedOnUtc")]
		public DateTime UpdatedOnUtc { get; set; }

		public IList<Sinek_LocationLocalizedModel> Locales { get; set; }

		public bool HideNameAndDescriptionProperties { get; set; }
		public bool HidePublishedProperty { get; set; }
		public bool HideDisplayOrderProperty { get; set; }

		public virtual double Lat { get; set; }
		public virtual double Lng { get; set; }

		#endregion
	}

	public class Sinek_LocationLocalizedModel : ILocalizedLocaleModel
	{
		public int LanguageId { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Locations.Fields.Latitude")]
		public double Latitude { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Locations.Fields.Longitude")]
		public double Longitude { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Locations.Fields.LocationType")]
		public string LocationType { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Locations.Fields.Id")]
		public int Id { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Locations.Fields.Name1")]
		public string Name1 { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Locations.Fields.Name2")]
		public string Name2 { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Locations.Fields.LocationAddress")]
		public string LocationAddress { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Locations.Fields.PhoneNo1")]
		public string PhoneNo1 { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Locations.Fields.PhoneNo2")]
		public string PhoneNo2 { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Locations.Fields.Fax")]
		public string Fax { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Locations.Fields.WebSite")]
		public string WebSite { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Locations.Fields.EMail")]
		public string EMail { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Locations.Fields.Notes")]
		public string Notes { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Locations.Fields.SellsSk8tape")]
		public bool SellsSk8tape { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Locations.Fields.SellsRockerz")]
		public bool SellsRockerz { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Locations.Fields.PageSize")]
		public int PageSize { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Locations.Fields.PageSizeOptions")]
		public string PageSizeOptions { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Locations.Fields.Published")]
		public bool Published { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Locations.Fields.Deleted")]
		public bool Deleted { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Locations.Fields.CreatedOnUtc")]
		public DateTime CreatedOnUtc { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Locations.Fields.UpdatedOnUtc")]
		public DateTime UpdatedOnUtc { get; set; }

		public virtual double Lat { get; set; }
		public virtual double Lng { get; set; }

	}
}
