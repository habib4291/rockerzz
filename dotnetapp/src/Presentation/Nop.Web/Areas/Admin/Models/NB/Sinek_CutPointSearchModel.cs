﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.NB
{
	/// <summary>
	/// Represents a sinek cutpoint search model
	/// </summary>
	public partial class Sinek_CutPointSearchModel : BaseSearchModel
    {
        #region Ctor

        public Sinek_CutPointSearchModel()
        {
        }

        #endregion

        #region Properties

        #endregion
    }
}
