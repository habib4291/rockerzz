﻿using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Web.Areas.Admin.Models.NB
{
	/// <summary>
	/// Represents a sinek location search model
	/// </summary>
	public partial class Sinek_LocationSearchModel : BaseSearchModel
	{
		#region Ctor

		public Sinek_LocationSearchModel()
		{
		}

		#endregion

		#region Properties

		[NopResourceDisplayName("Admin.Catalog.SinekLocation.Search.Fields.Name1")]
		public string Name1 { get; set; }

		[NopResourceDisplayName("Admin.Catalog.SinekLocation.Search.Fields.Name2")]
		public string Name2 { get; set; }

		#endregion
	}
}
