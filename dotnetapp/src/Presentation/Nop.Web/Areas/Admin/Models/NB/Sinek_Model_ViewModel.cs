﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;
using System;
using System.Collections.Generic;

namespace Nop.Web.Areas.Admin.Models.NB
{
	/// <summary>
	/// Represents a SinekModelView model
	/// </summary>
	public class Sinek_Model_ViewModel : BaseNopEntityModel, ILocalizedModel<Sinek_Model_ViewLocalizedModel>
	{
		#region Ctor

		public Sinek_Model_ViewModel()
		{
			if (PageSize < 1)
			{
				PageSize = 5;
			}
			Locales = new List<Sinek_Model_ViewLocalizedModel>();
			AvailableManufacturerId = new List<SelectListItem>();
		}

		#endregion

		#region Properties

		[NopResourceDisplayName("Admin.Catalog.Sinek_Models_View.Fields.Name")]
		public string Name { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Models_View.Fields.Description")]
		public string Description { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Models_View.Fields.PageSize")]
		public int PageSize { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Models_View.Fields.PageSizeOptions")]
		public string PageSizeOptions { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Models_View.Fields.Published")]
		public bool Published { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Models_View.Fields.Deleted")]
		public bool Deleted { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Models_View.Fields.CreatedOnUtc")]
		public DateTime CreatedOnUtc { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Models_View.Fields.UpdatedOnUtc")]
		public DateTime UpdatedOnUtc { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Models_View.Fields.ManufacturerId")]
		public int ManufacturerId { get; set; }
		public IList<SelectListItem> AvailableManufacturerId { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Models_View.Fields.ManufacturerName")]
		public string ManufacturerName { get; set; }


		[NopResourceDisplayName("Admin.Catalog.Sinek_Models_View.Fields.DisplayOrder")]
		public int DisplayOrder { get; set; }


		public IList<Sinek_Model_ViewLocalizedModel> Locales { get; set; }

		public bool HideNameAndDescriptionProperties { get; set; }
		public bool HidePublishedProperty { get; set; }
		public bool HideDisplayOrderProperty { get; set; }

		#endregion

	}

	public class Sinek_Model_ViewLocalizedModel : ILocalizedLocaleModel
	{
		public int LanguageId { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Models_View.Fields.Id")]
		public int Id { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Models_View.Fields.Name")]
		public string Name { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Models_View.Fields.Description")]
		public string Description { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Models_View.Fields.PageSize")]
		public int PageSize { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Models_View.Fields.PageSizeOptions")]
		public string PageSizeOptions { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Models_View.Fields.Published")]
		public bool Published { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Models_View.Fields.Deleted")]
		public bool Deleted { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Models_View.Fields.CreatedOnUtc")]
		public DateTime CreatedOnUtc { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Models_View.Fields.UpdatedOnUtc")]
		public DateTime UpdatedOnUtc { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Models_View.Fields.ManufacturerId")]
		public int ManufacturerId { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Models_View_View.Fields.ManufacturerName")]
		public string ManufacturerName { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Models_View_View.Fields.DisplayOrder")]
		public int DisplayOrder { get; set; }
	}
}
