﻿using FluentValidation;
using Nop.Core.Domain.NB;
using Nop.Data;
using Nop.Services.Localization;
using Nop.Web.Areas.Admin.Models.NB;
using Nop.Web.Framework.Validators;

namespace Nop.Web.Areas.Admin.Validators.NB
{
    public partial class Sinek_ModelValidator : BaseNopValidator<Sinek_ModelModel>
    {
        public Sinek_ModelValidator(ILocalizationService localizationService, IDbContext dbContext)
        {
            RuleFor(x => x.Name).NotNull().WithMessage(localizationService.GetResource("Admin.Catalog.Sinek_Model.Fields.Name.Required"));
            
            SetDatabaseValidationRules<Sinek_Model>(dbContext);
        }
    }
}
