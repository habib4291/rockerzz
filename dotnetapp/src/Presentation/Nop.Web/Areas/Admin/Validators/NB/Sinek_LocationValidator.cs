﻿using FluentValidation;
using Nop.Core.Domain.NB;
using Nop.Data;
using Nop.Services.Localization;
using Nop.Web.Areas.Admin.Models.NB;
using Nop.Web.Framework.Validators;

namespace Nop.Web.Areas.Admin.Validators.NB
{
    public partial class Sinek_LocationValidator : BaseNopValidator<Sinek_LocationModel>
    {
        public Sinek_LocationValidator(ILocalizationService localizationService, IDbContext dbContext)
        {
            RuleFor(x => x.LocationAddress).NotNull().WithMessage(localizationService.GetResource("Admin.Catalog.Sinek_Locations.Fields.LocationAddress.Required"));

            SetDatabaseValidationRules<Sinek_Location>(dbContext);
        }
    }
}
