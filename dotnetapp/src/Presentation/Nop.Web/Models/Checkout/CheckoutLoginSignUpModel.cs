﻿using System.ComponentModel.DataAnnotations;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Web.Models.Checkout
{
	public partial class CheckoutLoginSignUpModel : BaseNopModel
    {
        public CheckoutLoginSignUpModel()
        {
            SignUpModel = new SignUpModel();
        }

        public string PhoneNumber { get; set; }
        public string PhoneOrEmail { get; set; }
        public string Password { get; set; }
        public bool IsEmail { get; set; }
        public bool IsLogedIn { get; set; }
        public bool IsValid { get; set; }
        public string Message{ get; set; }

        public SignUpModel SignUpModel { get; set; }

    }

    public partial class SignUpModel : BaseNopModel
    {
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [DataType(DataType.Password)]
        [NoTrim]
        public string Password { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }

        [DataType(DataType.PhoneNumber)]
        public string Phone { get; set; }

        public bool IsValid { get; set; }
        public int CustomerId { get; set; }
        public string Message { get; set; }
    }
}