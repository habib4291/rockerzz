﻿using Nop.Core.Infrastructure;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace Nop.Web.Models.Catalog
{
	public class LocationModel : List<Location>
	{
		public LocationModel()
		{
			var fileProvider = EngineContext.Current.Resolve<INopFileProvider>();
			// Linq to read XML data and create a List of Location objects
			XDocument locationMetaData = XDocument.Load(fileProvider.Combine(fileProvider.MapPath("~/App_Data/"), "dataEmployee.xml"));
			var locations = from location in locationMetaData.Descendants("Employee")
							select new Location((int)location.Element("Id"), location.Element("Name1").Value, location.Element("LocationAddress").Value);
			this.AddRange(locations.ToList<Location>());
		}

	}
}