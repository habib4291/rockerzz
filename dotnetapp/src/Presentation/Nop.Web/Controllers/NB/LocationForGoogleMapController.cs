﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using Nop.Core;
using Nop.Core.Data;
using Nop.Web.Models.Catalog;
using System.Net;
using Nop.Services.Configuration;
using Microsoft.AspNetCore.Mvc;
using Nop.Core.Infrastructure;

namespace Nop.Web.Controllers
{
	public class LocationForGoogleMapController : BasePublicController
    {
        DataTable dt;
        LocationModel allLocs = new LocationModel();
        private readonly IWebHelper _webHelper;
        private readonly ISettingService _settingService;


        public LocationForGoogleMapController(
            IWebHelper webHelper, 
            ISettingService settingService)
        {
            this._webHelper = webHelper;
            this._settingService = settingService;
        }

        public IActionResult Index()
        {
            return View(allLocs);
        }

        public IActionResult GoogleMaps()
        {
            Location loc = this.getLocation(1);

            // if the id does not match any location return error page
            if (loc == null)
            {
                return View("Error");
            }
            else
            {
                if (this.lookupGeoCode(loc))
                {
                    return View(loc);
                }

                else
                {
                    return View("Error");
                }
            }
        }


        private Location getLocation(int id)
        {
            Location result = allLocs.Find(item => item.ID == id);
            return result;
        }

        public Boolean lookupGeoCode(Location loc)
        {
            var googleKey = _settingService.GetSettingByKey<string>("NB.GoogleAccount.Key", "AIzaSyArHFwUN6epZiqDMAzdp-uhjgbmAJ_uttc");
            string PostUrl = "https://maps.googleapis.com/maps/api/geocode/xml?address=" + loc.LocationAddress + "&key=" + googleKey + "&sensor=false";
            try
            {
                XDocument geo = XDocument.Load(PostUrl);
                // if an error occurs with the GeoCoding API return false         
                var Lat = geo.Descendants("result").FirstOrDefault().Descendants("geometry").FirstOrDefault().Descendants("location").FirstOrDefault().Element("lat").Value;
                var Lng = geo.Descendants("result").FirstOrDefault().Descendants("geometry").FirstOrDefault().Descendants("location").FirstOrDefault().Element("lng").Value;
                loc.Lat = Convert.ToDouble(Lat);

                loc.Lng = Convert.ToDouble(Lng);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public IActionResult GetMap()
        {
            List<MapInfo> Res = new List<MapInfo>();

            #region Here manage lat and lng on behalf of location details
            Core.Data.DataSettingsManager dataSettingsManager = new DataSettingsManager();
            var conn = new SqlConnectionStringBuilder(DataSettingsManager.LoadSettings().DataConnectionString);
            SqlConnection scCon = new SqlConnection();
            scCon.ConnectionString = conn.ToString();
            scCon.Open();
            string sql = "SELECT LocationAddress FROM Sinek_Location where Deleted = 0 and Published = 1";
            SqlDataAdapter da = new SqlDataAdapter(sql, scCon);
            DataTable dt = new DataTable();
            da.Fill(dt);
            scCon.Close();

            if (dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string PostUrl = "http://maps.googleapis.com/maps/api/geocode/xml?address=" + dt.Rows[i]["LocationAddress"].ToString().Trim() + " &sensor=false";

                    XDocument geo = XDocument.Load(PostUrl);

                    double Lat = (double)(from geocode in geo.Descendants("location")
                                          select geocode.Element("lat")).SingleOrDefault();

                    double Lng = (double)(from geocode in geo.Descendants("location")
                                          select geocode.Element("lng")).SingleOrDefault();
                    string status = Convert.ToString((from geocode in geo.Descendants("GeocodeResponse")
                                                      select geocode.Element("status")).SingleOrDefault());

                    if (status != "<status>ZERO_RESULTS</status>")
                    {
                        MapInfo _entity = new MapInfo();
                        //_entity.status = status;
                        _entity.address = dt.Rows[i]["LocationAddress"].ToString().Trim();
                        _entity.Lat = Lat;
                        _entity.Lag = Lng;
                        _entity.sno = i + 1;
                        Res.Add(_entity);
                    }
                }

            }

            #endregion
            return Json(Res.ToList());
        }



        public IActionResult GetMapByIpAddress()
        {
            // we use following url for get client IP
            //http://jsonip.appspot.com/?callback=getip
            //http://api.hostip.info/get_html.php
            string strNVP = string.Empty;
            List<MapInfo> Res = new List<MapInfo>();
            int Zoom = 0;
            double CenterLat = 0;
            double CenterLag = 0;
            string defaultCountry = "";

            #region Here manage lat and lng on behalf of location details
            Core.Data.DataSettingsManager dataSettingsManager = new DataSettingsManager();
            var conn = new SqlConnectionStringBuilder(DataSettingsManager.LoadSettings().DataConnectionString);
            SqlConnection scCon = new SqlConnection();
            scCon.ConnectionString = conn.ToString();
            scCon.Open();
            string sql = "SELECT LocationAddress,Latitude,Longitude,Name1,Name2,PhoneNo1,PhoneNo2,Fax,WebSite,EMail " +
            "FROM Sinek_Location where Deleted = 0 and Published = 1";
            SqlDataAdapter da = new SqlDataAdapter(sql, scCon);
            DataTable dt = new DataTable();
            da.Fill(dt);
            scCon.Close();


            #region Get Location By IP address Or Default IP Address
            string ipaddress = "";

            ipaddress = _webHelper.GetCurrentIpAddress();
            string[] arr = GetDetailsBrowserRelated(ipaddress).Split(new char[] { ';' });//client ip address
            if (arr[0] == "OK" && Convert.ToString(arr[5]) != "-")
            {
                Zoom = 6;
                CenterLat = Convert.ToDouble(arr[8]);
                CenterLag = Convert.ToDouble(arr[9]);
            }

            else if (arr[0] == "OK" && Convert.ToString(arr[5]) == "-")
            {
                Zoom = 4;// Client default address
                CenterLat = 39.828175;// Client default Lat
                CenterLag = -98.5795;// Client default Lag
            }
            #endregion

            if (dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    //string PostUrl = "http://maps.googleapis.com/maps/api/geocode/xml?address=" + dt.Rows[i]["LocationAddress"].ToString().Trim() + " &sensor=true";

                    //XDocument geo = XDocument.Load(PostUrl);

                    //string status = Convert.ToString((from geocode in geo.Descendants("GeocodeResponse")
                    //                                  select geocode.Element("status")).SingleOrDefault());

                    //if (status == "<status>OVER_QUERY_LIMIT</status>")
                    //{
                    //    Thread.Sleep(50000);
                    //    //GetGeocodingSearchResults(address);
                    //    PostUrl = "http://maps.googleapis.com/maps/api/geocode/xml?address=" + dt.Rows[i]["LocationAddress"].ToString().Trim() + " &sensor=true";


                    //     geo = XDocument.Load(PostUrl);


                    //    status = Convert.ToString((from geocode in geo.Descendants("GeocodeResponse")
                    //                                      select geocode.Element("status")).SingleOrDefault());
                    //}
                    //if (status == "<status>OK</status>") //&& status != "<status>ZERO_RESULTS</status>")
                    //{
                    //    var q = (from b in geo.Descendants("location")
                    //            select new
                    //            {
                    //                Lat = (double?)b.Element("lat"),
                    //                Lng = (double?)b.Element("lng")
                    //            }).ToList();                        

                    //    MapInfo _entity = new MapInfo();
                    //    _entity.address = dt.Rows[i]["LocationAddress"].ToString().Trim();

                    //    _entity.Lat = Convert.ToDouble(q[0].Lat);
                    //    _entity.Lag = Convert.ToDouble(q[0].Lng);
                    //    //_entity.IPAddressLag = defaultLag;
                    //    //_entity.IPAddressLat = defaultLat;
                    //    //_entity.IPAddressOfBrowser = ipaddress;
                    //    _entity.sno = i + 1;
                    //    Res.Add(_entity);
                    //}

                    MapInfo _entity = new MapInfo();
                    _entity.address = dt.Rows[i]["LocationAddress"].ToString().Trim();
                    _entity.Name1 = dt.Rows[i]["Name1"].ToString().Trim();
                    _entity.Name2 = dt.Rows[i]["Name2"].ToString().Trim();
                    _entity.PhoneNumber1 = dt.Rows[i]["PhoneNo1"].ToString().Trim();
                    _entity.PhoneNumber2 = dt.Rows[i]["PhoneNo2"].ToString().Trim();
                    _entity.Fax = dt.Rows[i]["Fax"].ToString().Trim();
                    _entity.WebSite = dt.Rows[i]["WebSite"].ToString().Trim();
                    _entity.email = dt.Rows[i]["EMail"].ToString().Trim();

                    _entity.Lat = Convert.ToDouble(dt.Rows[i]["Latitude"].ToString().Trim());
                    _entity.Lag = Convert.ToDouble(dt.Rows[i]["Longitude"].ToString().Trim());

                    _entity.sno = i + 1;
                    Res.Add(_entity);
                }
            }
            MapInfoList mil = new MapInfoList();
            mil.InfoList = Res;
            mil.Zoom = Zoom;
            mil.CenterLat = CenterLat;
            mil.CenterLag = CenterLag;
            #endregion
            return Json(mil);
        }

        public static string GetDetailsBrowserRelated(string ipAddress)
        {
            string myKey = "1f81f89abd41ff2912f9a053bfd2f1f4fd107bd3461338906f54d160ed662cf9";//"PUT YOUR KEY HERE";

            //Create a WebRequest
            WebRequest rssReq = WebRequest.Create("http://api.ipinfodb.com/v3/ip-city/?key=" + myKey + "&ip=" + ipAddress);

            //Create a Proxy
            WebProxy px = new WebProxy("http://api.ipinfodb.com/v3/ip-city/?key=" + myKey + "&ip=" + ipAddress, true);

            //Assign the proxy to the WebRequest
            rssReq.Proxy = px;

            //Set the timeout in Seconds for the WebRequest
            rssReq.Timeout = 10000;

            //Get the WebResponse 
            WebResponse rep = rssReq.GetResponse();

            //Read the Response in a XMLTextReader
            System.IO.StreamReader reader = new System.IO.StreamReader(rep.GetResponseStream());
            //string tmp = reader.ReadToEnd();
            return reader.ReadLine();
        }

        public static string GetIPAddress()
        {
            var webStaticHelper = EngineContext.Current.Resolve<IWebHelper>();
            return webStaticHelper.GetCurrentIpAddress();

            //string ip = string.Empty;
            //try
            //{
            //    ip = request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            //    if (!string.IsNullOrEmpty(ip))
            //    {
            //        if (ip.IndexOf(",") > 0)
            //        {
            //            string[] allIps = ip.Split(',');
            //            int le = allIps.Length - 1;
            //            ip = allIps[le];
            //        }
            //    }
            //    else
            //    {
            //        ip = request.UserHostAddress;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    throw ex;
            //}

            //return ip;
        }
        public static string getclientIP()
        {
            string result = string.Empty;

            //result = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            return result;
        }

        public IActionResult GetMap1()
        {
            List<MapInfo> Res = new List<MapInfo>();

            #region Here manage lat and lng on behalf of location details
            Core.Data.DataSettingsManager dataSettingsManager = new DataSettingsManager();
            var conn = new SqlConnectionStringBuilder(DataSettingsManager.LoadSettings().DataConnectionString);
            SqlConnection scCon = new SqlConnection();
            scCon.ConnectionString = conn.ToString();
            scCon.Open();
            string sql = "SELECT LocationAddress FROM Sinek_Location where Deleted = 0 and Published = 1";
            SqlDataAdapter da = new SqlDataAdapter(sql, scCon);
            DataTable dt = new DataTable();
            da.Fill(dt);
            scCon.Close();

            string ipaddress = _webHelper.GetCurrentIpAddress();

            //ipaddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            //if (ipaddress == "" || ipaddress == null)
            //    ipaddress = Request.ServerVariables["REMOTE_ADDR"];

            // If you run this project on your local host (your computer) then you need to uncomment the next line to avoid getting the UNKOWN IP address.
            // Otherwise, the next line should be commented out if you are running this project on a web server.
            // ipaddress = "74.125.45.100";	

            // Lookup geographic location using IP address
            //XmlTextReader XmlRdr = GetLocation("205.226.93.65");

            ipaddress = "205.226.93.65";
            string myKey = "1f81f89abd41ff2912f9a053bfd2f1f4fd107bd3461338906f54d160ed662cf9";//"PUT YOUR KEY HERE";

            //Create a WebRequest
            //WebRequest rssReq = WebRequest.Create("http://api.ipinfodb.com/v3/ip-city/?key=" + myKey + "&ip=" + ipaddress + "&format=xml");
            WebRequest rssReq = WebRequest.Create("http://api.ipinfodb.com/v3/ip-city/?key=" + myKey + "&ip=" + ipaddress);

            //Create a Proxy
            //WebProxy px = new WebProxy("http://api.ipinfodb.com/v3/ip-city/?key=" + myKey + "&ip=" + ipaddress + "&format=xml", true);
            WebProxy px = new WebProxy("http://api.ipinfodb.com/v3/ip-city/?key=" + myKey + "&ip=" + ipaddress, true);

            //Assign the proxy to the WebRequest
            rssReq.Proxy = px;

            //Set the timeout in Seconds for the WebRequest
            rssReq.Timeout = 2000;

            //Get the WebResponse 
            WebResponse rep = rssReq.GetResponse();

            //Read the Response in a XMLTextReader
            System.IO.StreamReader reader = new System.IO.StreamReader(rep.GetResponseStream());
            //string tmp = reader.ReadToEnd();
            string tmp = reader.ReadLine();

            string[] arr = tmp.Split(new char[] { ';' });
            tmp = arr[3];
            if (tmp != "" && tmp != "-")
            {


                MapInfo _entity = new MapInfo();
                _entity.address = "";
                _entity.Lat = 0;
                _entity.Lag = 0;
                _entity.sno = 0;
                Res.Add(_entity);
            }


            #endregion
            return Json(Res.ToList());
        }

        protected XmlTextReader GetLocation(string ipaddress)
        {

            // Register at ipinfodb.com for a free key and put it here
            string myKey = "1f81f89abd41ff2912f9a053bfd2f1f4fd107bd3461338906f54d160ed662cf9";//"PUT YOUR KEY HERE";

            //Create a WebRequest
            WebRequest rssReq = WebRequest.Create("http://api.ipinfodb.com/v3/ip-city/?key=" + myKey + "&ip=" + ipaddress + "&format=xml");

            //Create a Proxy
            WebProxy px = new WebProxy("http://api.ipinfodb.com/v3/ip-city/?key=" + myKey + "&ip=" + ipaddress + "&format=xml", true);

            //Assign the proxy to the WebRequest
            rssReq.Proxy = px;

            //Set the timeout in Seconds for the WebRequest
            rssReq.Timeout = 2000;
            try
            {
                //Get the WebResponse 
                WebResponse rep = rssReq.GetResponse();

                //Read the Response in a XMLTextReader
                System.IO.StreamReader reader = new System.IO.StreamReader(rep.GetResponseStream()); string tmp = reader.ReadToEnd();
                XmlTextReader xtr = new XmlTextReader(rep.GetResponseStream());
                return xtr;

            }
            catch
            {
                return null;
            }
        }

        //public class MapInfo
        //{
        //    public string address { get; set; }
        //    //public string status { get; set; }
        //    public double Lat { get; set; }
        //    public double Lag { get; set; }
        //    public int sno { get; set; }

        //}

        public class MapInfo
        {
            public string address { get; set; }
            public string Name1 { get; set; }
            public string Name2 { get; set; }
            public string PhoneNumber1 { get; set; }
            public string PhoneNumber2 { get; set; }
            public string Fax { get; set; }
            public string WebSite { get; set; }
            public string email { get; set; }
            public double Lat { get; set; }
            public double Lag { get; set; }
            public int sno { get; set; }
        }
        public class MapInfoList
        {
            public List<MapInfo> InfoList { get; set; }
            public int Zoom { get; set; }
            public double CenterLat { get; set; }
            public double CenterLag { get; set; }
        }
    }
}
