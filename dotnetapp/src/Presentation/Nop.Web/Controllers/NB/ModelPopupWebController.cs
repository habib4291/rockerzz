﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Orders;
using Nop.Services.Catalog;
using Nop.Services.ExportImport;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Services.Security;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Security;
using System.Text;
using System.Collections;
using Nop.Web.Framework.Mvc.Filters;
using Nop.Services.NB;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Core.Domain.NB;
using Nop.Web.Models.ModelPopupWebs;

namespace Nop.Web.Controllers
{
	[HttpsRequirement(SslRequirement.No)]
    public partial class ModelPopupWebController : BasePublicController
    {
        #region Fields

        private readonly ISinek_CutPointService _sinek_CutPointService;
        private readonly IManufacturerService _manufacturerService;
        private readonly ISinek_ModelService _sinek_ModelService;
        private readonly ILanguageService _languageService;
        private readonly ILocalizationService _localizationService;
        private readonly ILocalizedEntityService _localizedEntityService;
        private readonly IExportManager _exportManager;
        private readonly IPermissionService _permissionService;
        private readonly AdminAreaSettings _adminAreaSettings;
        private readonly CatalogSettings _catalogSettings;
        private readonly IProductAttributeService _productAttributeService;
        private readonly IWorkContext _workContext;
        private readonly IProductAttributeParser _productAttributeParser;
        private readonly IPictureService _pictureService;

        #endregion

        #region Constructors

        public ModelPopupWebController(IManufacturerService manufacturerService,
            ISinek_ModelService sinek_ModelService,
            ILanguageService languageService,
            ILocalizationService localizationService,
            //ILocalizedEntityService localizedEntityService,
            IExportManager exportManager,
            AdminAreaSettings adminAreaSettings,
            CatalogSettings catalogSettings,
            ISinek_CutPointService sinek_CutPointService,
            IPermissionService permissionService,
            IProductAttributeService productAttributeService,
            IWorkContext workContext,
            IProductAttributeParser productAttributeParser,
            IPictureService pictureService)
        {
            this._pictureService = pictureService;
            this._manufacturerService = manufacturerService;
            this._sinek_ModelService = sinek_ModelService;
            this._languageService = languageService;
            this._localizationService = localizationService;
            //this._localizedEntityService = localizedEntityService;
            this._exportManager = exportManager;
            this._adminAreaSettings = adminAreaSettings;
            this._catalogSettings = catalogSettings;
            this._sinek_CutPointService = sinek_CutPointService;
            this._permissionService = permissionService;
            this._productAttributeService = productAttributeService;
            this._workContext = workContext;
            this._productAttributeParser = productAttributeParser;
        }

        #endregion

        public IActionResult Create()
        {
            var model = new Sinek_CutPointWebModel();
            //locales
            //default values
            //model.PageSize = 4;
            //model.Published = true;

            //model.PageSizeOptions = _catalogSettings.DefaultSinek_CutPointPageSizeOptions;

            var pictures = _pictureService.GetPicturesByProductId(13);
            var selectedPicture = (from p in pictures select p).LastOrDefault();
            model.ModalPictureModel.ImageUrl = _pictureService.GetPictureUrl(selectedPicture, 300);
            model.ModalPictureModel.FullSizeImageUrl = _pictureService.GetPictureUrl(selectedPicture);


            //model.AvailableManufacturerId.Add(new SelectListItem() { Text = "Select Blade Make", Value = "0" });
            var Manufactures = _manufacturerService.GetAllManufacturers();
            var cnt = 0;
            if (Manufactures.Count > 0)
            {
                ViewBag.FirstManufacturerId = Manufactures.FirstOrDefault().Id.ToString();
                model.ManufacturerId = Manufactures.FirstOrDefault().Id;
                foreach (var c in Manufactures)
                {
                    model.AvailableManufacturerId.Add(new SelectListItem() { Text = c.Name, Value = c.Id.ToString() });

                    cnt += cnt;
                }
                if (cnt == 0)
                {

                    var sinek_ModelInner = _sinek_ModelService.GetSinek_ModelByManufacturerId(model.ManufacturerId).ToList();
                    if (sinek_ModelInner.Count > 0)
                    {
                        model.ModelId = sinek_ModelInner.FirstOrDefault().Id;
                        foreach (var s in sinek_ModelInner)
                        {
                            model.AvailableModelId.Add(new SelectListItem() { Text = s.Name, Value = s.Id.ToString(), Selected = (s.Id == model.ModelId) });
                        }
                        var sinek_CutPointInner = _sinek_CutPointService.GetSinek_CutPointByManufacturerIdModelId(model.ModelId, model.ManufacturerId).ToList();
                        foreach (var s in sinek_CutPointInner)
                            model.AvailableCutPoint.Add(new SelectListItem() { Text = s.Size, Value = s.Id.ToString(), Selected = (s.Id == model.Id) });
                    }
                }
            }



            //var sinek_Model = _sinek_ModelService.GetSinek_ModelByManufacturerId(model.ManufacturerId).ToList();
            //if (sinek_Model.Count > 0)
            //{
            //    //model.AvailableModelId.Add(new SelectListItem() { Text = "Select Blade Model", Value = "0" });
            //    foreach (var s in sinek_Model)
            //        model.AvailableModelId.Add(new SelectListItem() { Text = s.Name, Value = s.Id.ToString(), Selected = (s.Id == model.ModelId) });
            //}

            //var sinek_CutPoint = _sinek_CutPointService.GetSinek_CutPointByManufacturerIdModelId(model.ModelId, model.ManufacturerId).ToList();
            //if (sinek_CutPoint.Count > 0)
            //{
            //    foreach (var s in sinek_CutPoint)
            //        model.AvailableCutPoint.Add(new SelectListItem() { Text = s.CutPoint });
            //}

            //var sinek_cutPoint = _sinek_CutPointService.GetSinek_CutPointByManufacturerIdModelId(model.ModelId, model.ManufacturerId).ToList();
            //if (sinek_cutPoint.Count > 0)
            //{
            //    model.AvailableCutPoint.Add(new SelectListItem() { Text = "---", Value = "0" });
            //    foreach (var s in sinek_cutPoint)
            //        model.AvailableCutPoint.Add(new SelectListItem() { Text = s.CutPoint, Value = s.Id.ToString(), Selected = (s.Id == model.Id) });
            //}
            return View(model);

        }

        //TODO nz pending
        [HttpPost, FormValueRequired("save", "save-continue", "continueEditing")]
        public IActionResult Create(Sinek_CutPointWebModel model, bool continueEditing)
        {
            var cntModel = 0;
            var cntSize = 0;
            //model.AvailableManufacturerId.Add(new SelectListItem() { Text = "Select Blade Make", Value = "0" });

            foreach (var c in _manufacturerService.GetAllManufacturers())
            {
                model.AvailableManufacturerId.Add(new SelectListItem() { Text = c.Name, Value = c.Id.ToString() });

                if (cntModel == 0)
                {
                    ViewBag.FirstManufacturerId = c.Id.ToString();
                    var sinek_ModelInner = _sinek_ModelService.GetSinek_ModelByManufacturerId(c.Id).ToList();
                    foreach (var s in sinek_ModelInner)
                    {
                        model.AvailableModelId.Add(new SelectListItem() { Text = s.Name, Value = s.Id.ToString(), Selected = (s.Id == model.ModelId) });

                        if (cntSize == 0)
                        {
                            var sinek_CutPointInner = _sinek_CutPointService.GetSinek_CutPointByManufacturerIdModelId(s.Id, c.Id).ToList();
                            foreach (var t in sinek_CutPointInner)
                                model.AvailableCutPoint.Add(new SelectListItem() { Text = t.Size, Value = t.Id.ToString(), Selected = (t.Id == model.Id) });
                        }

                        cntSize += 1;
                    }
                }
                cntModel += 1;
            }

            var pictures = _pictureService.GetPicturesByProductId(13);
            var prodpics = (from p in pictures select p);
            if (prodpics.Count() > 1)
            {
                var selectedPicture = (from p in pictures select p).LastOrDefault();
                model.ModalPictureModel.ImageUrl = _pictureService.GetPictureUrl(selectedPicture, 300);
                model.ModalPictureModel.FullSizeImageUrl = _pictureService.GetPictureUrl(selectedPicture);
                //var selectedPicture = (from p in pictures  select p).LastOrDefault();
                //model.ModalPictureModel.ImageUrl = _pictureService.GetPictureUrl(selectedPicture, 300);
                //model.ModalPictureModel.FullSizeImageUrl = _pictureService.GetPictureUrl(selectedPicture);
            }
            else
            {
                model.ModalPictureModel.ImageUrl = "";
                model.ModalPictureModel.FullSizeImageUrl = "";
            }


            return View(model);

        }

        [HttpsRequirement(SslRequirement.No)]
        public IActionResult GetSinek_ModelByManufacturerId(string manufacturerId, bool addEmptyModelIfRequired)
        {
            //manufacturerId = "1";
            // This action method gets called via an ajax request
            if (String.IsNullOrEmpty(manufacturerId))
                throw new ArgumentNullException("manufacturerId");

            var manufacturer = _manufacturerService.GetManufacturerById(Convert.ToInt32(manufacturerId));
            var sinek_Models = manufacturer != null ? _sinek_ModelService.GetSinek_ModelByManufacturerId(manufacturer.Id).ToList() : new List<Sinek_Model>();
            var result = (from s in sinek_Models
                          select new { id = s.Id, name = _localizationService.GetLocalized(s, entity => entity.Name, _workContext.WorkingLanguage.Id, true, false) }).ToList();
            //if (addEmptyModelIfRequired && result.Count == 0)
            //    result.Insert(0, new { id = 0, name = _localizationService.GetResource("Address.OtherNonUS") });
            return Json(result);
        }

        [HttpsRequirement(SslRequirement.No)]
        public IActionResult GetSinek_CutPointByManufacturerIdModelId(string modelId, string manufacturerId, bool addEmptyModelIfRequired)
        {
            //manufacturerId = "1";
            // This action method gets called via an ajax request
            if (String.IsNullOrEmpty(manufacturerId) && String.IsNullOrEmpty(modelId))
                throw new ArgumentNullException("modelId");

            var manufacturer = _manufacturerService.GetManufacturerById(Convert.ToInt32(manufacturerId));
            var sinek_Model = _sinek_ModelService.GetSinek_ModelById(Convert.ToInt32(modelId));
            var sinek_CutPoints = manufacturer != null && sinek_Model != null ? _sinek_CutPointService.GetSinek_CutPointByManufacturerIdModelId(sinek_Model.Id, manufacturer.Id).ToList() : new List<Sinek_CutPoint>();
            var result = (from s in sinek_CutPoints
                          select new { id = s.CutPoint, name = _localizationService.GetLocalized(s, entity => entity.Size, _workContext.WorkingLanguage.Id, true, false) }).ToList();
            //if (addEmptyModelIfRequired && result.Count == 0)
            //    result.Insert(0, new { id = 0, name = _localizationService.GetResource("Address.OtherNonUS") });
            return Json(result);
        }

        [HttpsRequirement(SslRequirement.No)]
        public IActionResult GetSinek_CutPointByModelIId(string modelId, string manufacturerId, bool addEmptyModelIfRequired)
        {
            //manufacturerId = "1";
            // This action method gets called via an ajax request
            if (String.IsNullOrEmpty(manufacturerId) && String.IsNullOrEmpty(modelId))
                throw new ArgumentNullException("modelId");

            var manufacturer = _manufacturerService.GetManufacturerById(Convert.ToInt32(manufacturerId));
            var sinek_Model = _sinek_ModelService.GetSinek_ModelById(Convert.ToInt32(modelId));
            var sinek_CutPoints = manufacturer != null && sinek_Model != null ? _sinek_CutPointService.GetSinek_CutPointByModelIId(sinek_Model.Id, manufacturer.Id).ToList() : new List<Sinek_CutPoint>();
            var result = (from s in sinek_CutPoints
                          select new { id = s.Id, name = _localizationService.GetLocalized(s, entity => entity.Size, _workContext.WorkingLanguage.Id, true, false) }).ToList();
            //if (addEmptyModelIfRequired && result.Count == 0)
            //    result.Insert(0, new { id = 0, name = _localizationService.GetResource("Address.OtherNonUS") });
            return Json(result);
        }

        public IActionResult GetSinek_CutPointByManufacturerIdModelIdSize(string modelId, string manufacturerId, string size, bool addEmptyModelIfRequired)
        {
            //manufacturerId = "1";
            // This action method gets called via an ajax request
            if (String.IsNullOrEmpty(manufacturerId) && String.IsNullOrEmpty(modelId) && String.IsNullOrEmpty(size))
                throw new ArgumentNullException("modelId");

            var manufacturer = _manufacturerService.GetManufacturerById(Convert.ToInt32(manufacturerId));
            var sinek_Model = _sinek_ModelService.GetSinek_ModelById(Convert.ToInt32(modelId));
            //var size = _sinek_ModelService.GetSinek_ModelById(Convert.ToString(size));
            var sinek_CutPoints = manufacturer != null && sinek_Model != null && size != null ? _sinek_CutPointService.GetSinek_CutPointByManufacturerIdModelIdSize(sinek_Model.Id, manufacturer.Id, size).ToList() : new List<Sinek_CutPoint>();
            var result = (from s in sinek_CutPoints
                          select new { id = s.Id, name = _localizationService.GetLocalized(s, entity => entity.CutPoint, _workContext.WorkingLanguage.Id, true, false) }).ToList();
            //if (addEmptyModelIfRequired && result.Count == 0)
            //    result.Insert(0, new { id = 0, name = _localizationService.GetResource("Address.OtherNonUS") });
            return Json(result);

        }

        private void setDesignMineGuardColorsCount(string color1, string color2, string color3, string color4,
            ref int count1, ref int count2, ref int count3, ref int count4)
        {
            count1 = countColors(color1, color1, color2, color3, color4);
            count2 = countColors(color2, color1, color2, color3, color4);
            count3 = countColors(color3, color1, color2, color3, color4);
            count4 = countColors(color4, color1, color2, color3, color4);
        }
        private int countColors(string checkColor, string color1, string color2, string color3, string color4)
        {
            int result = 0;
            if (checkColor == color1)
                result++;
            if (checkColor == color2)
                result++;
            if (checkColor == color3)
                result++;
            if (checkColor == color4)
                result++;
            return result;
        }
        private void GetStatusOfColors(string color, int inv, int count, ref StringBuilder sb, ref bool flag)
        {

        }
        public ContentResult CheckStock(string color1, string color2, string color3, string color4, string productVariantId)
        {
            string result = "";
            try
            {
                bool flag = false;

                int count1 = 0; int count2 = 0; int count3 = 0; int count4 = 0;
                int inv1 = 0; int inv2 = 0; int inv3 = 0; int inv4 = 0;

                var combinations = _productAttributeService.GetAllProductAttributeCombinations(Convert.ToInt32(productVariantId));
                var pvaId = _productAttributeService.GetProductAttributeMappingsByProductId(Convert.ToInt32(productVariantId)).Where(a => a.TextPrompt == "Guard Colors").ToList();
                int _pvaId = pvaId[0].Id;
                var pvaValues = _productAttributeService.GetProductAttributeValues(_pvaId);

                // Check and count similar colours count in design mine
                setDesignMineGuardColorsCount(color1, color2, color3, color4, ref count1, ref count2, ref count3, ref count4);

                // Stock quantity in combinations table
                var _getColor = pvaValues.Where(pp => pp.Name == color1).ToList();
                if (_getColor.Count > 0)
                {
                    var _getCombination = combinations.Where(c => c.AttributesXml.Contains(_getColor[0].Id.ToString())).ToList();
                    if (_getCombination.Count > 0)
                        inv1 = _getCombination[0].StockQuantity;
                }
                //inv1 = combinations.Where(c => c.AttributesXml.Contains(pvaValues.Where(pp => pp.Name == color1).ToList()[0].Id.ToString())).ToList()[0].StockQuantity;
                _getColor = pvaValues.Where(pp => pp.Name == color2).ToList();
                if (_getColor.Count > 0)
                {
                    var _getCombination = combinations.Where(c => c.AttributesXml.Contains(_getColor[0].Id.ToString())).ToList();
                    if (_getCombination.Count > 0)
                        inv2 = _getCombination[0].StockQuantity;
                }
                //inv2 = combinations.Where(c => c.AttributesXml.Contains(pvaValues.Where(pp => pp.Name == color2).ToList()[0].Id.ToString())).ToList()[0].StockQuantity;
                _getColor = pvaValues.Where(pp => pp.Name == color3).ToList();
                if (_getColor.Count > 0)
                {
                    var _getCombination = combinations.Where(c => c.AttributesXml.Contains(_getColor[0].Id.ToString())).ToList();
                    if (_getCombination.Count > 0)
                        inv3 = _getCombination[0].StockQuantity;
                }
                //inv3 = combinations.Where(c => c.AttributesXml.Contains(pvaValues.Where(pp => pp.Name == color3).ToList()[0].Id.ToString())).ToList()[0].StockQuantity;
                _getColor = pvaValues.Where(pp => pp.Name == color4).ToList();
                if (_getColor.Count > 0)
                {
                    var _getCombination = combinations.Where(c => c.AttributesXml.Contains(_getColor[0].Id.ToString())).ToList();
                    if (_getCombination.Count > 0)
                        inv4 = _getCombination[0].StockQuantity;
                }
                //inv4 = combinations.Where(c => c.AttributesXml.Contains(pvaValues.Where(pp => pp.Name == color4).ToList()[0].Id.ToString())).ToList()[0].StockQuantity;

                // Is any color out of stock
                ArrayList arr = new ArrayList();
                if (inv1 < count1)
                {
                    flag = true; arr.Add(color1);
                }
                if (inv2 < count2)
                {
                    flag = true; arr.Add(color2);
                }
                if (inv3 < count3)
                {
                    flag = true; arr.Add(color3);
                }
                if (inv4 < count4)
                {
                    flag = true; arr.Add(color4);
                }

                // check out of quantity colors and create a list of out of stock quantities
                StringBuilder sb = new StringBuilder();

                #region Out of stock v1
                //sb.Append("<table border=\"0\" cellpadding=\"10\" cellspacing=\"0\" width=\"100%\">");
                //sb.Append("<tr><td align=\"right\"><a href=\"javascript:void();\" ");
                //sb.Append("onclick=\"hide('my-dialog'), hide('loading_overlay');\">Close</a></td></tr>");
                //sb.Append("<tr><td>Following colours are <span style='color: red'>out of stock</span></td></tr>");

                //sb.Append("<tr><td><ul>");
                //foreach (var item in arr.ToArray().Distinct().ToArray())
                //{
                //    sb.Append("<li>");
                //    sb.Append(item.ToString());
                //    sb.Append("</li>");
                //}
                //sb.Append("</ul></td></tr>");
                //sb.Append("</table>");
                #endregion

                #region Out of stock v2
                //sb.Append("<div class=\"colorclose\">");
                //sb.Append("<a class=\"colorcloseanchor\" href=\"javascript:void();\" onclick=\"hide('my-dialog'), hide('loading_overlay');\">X</a>");
                //sb.Append("</div>");
                //sb.Append("<div class=\"color_contents\">");
                //sb.Append("<div class=\"colorheading\">");
                //sb.Append("Following colors are out of stock");
                //sb.Append("</div>");
                //sb.Append("<ul>");
                //foreach (var item in arr.ToArray().Distinct().ToArray())
                //{
                //    sb.Append("<li>");
                //    sb.Append(item.ToString());
                //    sb.Append("</li>");
                //}
                //sb.Append("</ul>");
                //sb.Append("</div>");
                #endregion

                #region Out of stock v3
                sb.Append("Following colors are out of stock:\n\n");
                foreach (var item in arr.ToArray().Distinct().ToArray())
                {
                    sb.Append(item.ToString());
                    sb.Append("\n");
                }
                #endregion

                if (flag == true)
                    result = sb.ToString();
                else
                    result = "<!-- -->";

                sb.Length = 0; sb = null;
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return Content(result);
        }
        public ContentResult CheckStockCart()
        {
            string result = "";
            try
            {
                int productVariantId = 13;
                var combinations = _productAttributeService.GetAllProductAttributeCombinations(Convert.ToInt32(productVariantId));
                var __pvaId = _productAttributeService.GetProductAttributeMappingsByProductId(Convert.ToInt32(productVariantId)).Where(a => a.TextPrompt == "Guard Colors").ToList();
                int _pvaId = __pvaId[0].Id;// Guard Colors ID
                var pvaValues = _productAttributeService.GetProductAttributeValues(_pvaId);

                // all colors and quantity
                List<GuardColorsClass> G = new List<GuardColorsClass>();
                foreach (var item in pvaValues)
                {
                    GuardColorsClass Gi = new GuardColorsClass();
                    Gi.Color = item.Name;
                    var _getCombination = combinations.Where(c => c.AttributesXml.Contains(item.Id.ToString())).ToList();
                    if (_getCombination.Count > 0)
                        Gi.Stock = _getCombination[0].StockQuantity;
                    else
                        Gi.Stock = 0;
                    Gi.CartItemCount = 0;
                    Gi.Incart = false;
                    G.Add(Gi);
                }

                // all cart item colors and there quantity
                var cart = _workContext.CurrentCustomer.ShoppingCartItems.Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart).ToList();
                foreach (var sci in cart)
                {
                    var pvaCollection = _productAttributeParser.ParseProductAttributeMappings(sci.AttributesXml);
                    for (int i = 0; i < pvaCollection.Count; i++)
                    {
                        var pva = pvaCollection[i];
                        if (pva.TextPrompt == "Front Left" || pva.TextPrompt == "Front Right" ||
                            pva.TextPrompt == "Back Left" || pva.TextPrompt == "Back Right")
                        {
                            var valuesStr = _productAttributeParser.ParseValues(sci.AttributesXml, pva.Id);
                            if (valuesStr.Count > 0)
                            {
                                string valueStr = valuesStr[0];
                                string pvaAttribute = string.Empty;
                                int pvaId = 0;
                                if (int.TryParse(valueStr, out pvaId))
                                {
                                    var pvaValue = _productAttributeService.GetProductAttributeValueById(pvaId);
                                    if (pvaValue != null)
                                    {
                                        string GuardName = _localizationService.GetLocalized(pvaValue, entity => entity.Name, _workContext.WorkingLanguage.Id, true, false);
                                        foreach (var _gi in G)
                                        {
                                            if (_gi.Color == GuardName)
                                            {
                                                _gi.Incart = true;
                                                _gi.CartItemCount += sci.Quantity;
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                // Check if shopping cart items count is more than stock quantity
                bool flag = false;
                StringBuilder sb = new StringBuilder();
                sb.Append("Following colors are out of stock:\n\n");
                foreach (var __gi in G)
                {
                    if (__gi.Incart == true)
                    {
                        if (__gi.Stock < __gi.CartItemCount)
                        {
                            flag = true;
                            sb.Append(__gi.Color);
                            //sb.Append(" : Stock(");
                            //sb.Append(__gi.Stock);
                            //sb.Append(") , Cart (");
                            //sb.Append(__gi.CartItemCount);
                            //sb.Append(")");
                            sb.Append("\n");
                        }
                    }
                }

                if (flag == true)
                {
                    result = sb.ToString();
                }
                else
                {
                    result = "OK";
                }

                sb.Length = 0; sb = null;
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return Content(result);
        }

        public ContentResult getColors(string productVariantId)
        {
            string result = "";

            var pvaId = _productAttributeService.GetProductAttributeMappingsByProductId(Convert.ToInt32(productVariantId)).Where(a => a.TextPrompt == "Guard Colors").ToList();
            int _pvaId = pvaId[0].Id;
            var pvaValues = _productAttributeService.GetProductAttributeValues(_pvaId);

            bool flag = false;
            foreach (var item in pvaValues)
            {
                if (flag == false)
                    flag = true;
                else
                    result += ",";
                result += item.Name;
            }

            return Content(result);
        }
        public ContentResult getColorValues(string productVariantId)
        {
            string result = "";

            var pvaId = _productAttributeService.GetProductAttributeMappingsByProductId(Convert.ToInt32(productVariantId)).Where(a => a.TextPrompt == "Guard Colors").ToList();
            int _pvaId = pvaId[0].Id;
            var pvaValues = _productAttributeService.GetProductAttributeValues(_pvaId);

            bool flag = false;
            foreach (var item in pvaValues)
            {
                if (flag == false)
                    flag = true;
                else
                    result += ",";
                result += item.ColorCode;
            }

            return Content(result);
        }
    }

    public class GuardColorsClass
    {
        public string Color { get; set; }
        public int Stock { get; set; }
        public int CartItemCount { get; set; }
        public bool Incart { get; set; }
    }
}
