
function leftNavActive() {
	var url = this.location.href;
	var activePage = url.substring(url.lastIndexOf('/') + 1);
	$('.block ul.list.list2 a').each(function () {
		var currentPage = this.href.substring(this.href.lastIndexOf('/') + 1);
		$(this).removeClass('active');
		if (activePage == currentPage) {
			if (!$(this).hasClass('active')) {
				$(".block ul.list.list2 a.active").removeClass("active");
				$(this).addClass("active");
			}
		}
	});
}


(function () {

	var stickyTopSections = $('.checkOutC1');

	var stickyTop = function () {
		var scrollTop = $(window).scrollTop();
		$('.checkOutC1').each(function () {
			var $this = $(this);
			if (scrollTop > 200 && scrollTop < 500) {
				$this.addClass('sticky');
			}
			else {
				$this.removeClass('sticky');
			}
		});
	};

	stickyTop();

	$(window).scroll(function () {
		stickyTop();
	});

});

 function addressTypeNav() {
	$(".address_type ul li").first().addClass('active');
	$('[name=addressType]').click(function () {
		var value = $(this).val();
		$('[name=addressType]').removeClass('active');
		$(this).addClass('active');
		$('#@Html.IdFor(x=>x.AddressTypeId)').val(value);
		$(this).parents('li').addClass('active').siblings().removeClass('active');
	});                      
} 