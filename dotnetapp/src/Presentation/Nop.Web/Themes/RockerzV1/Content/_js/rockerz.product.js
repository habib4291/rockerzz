﻿/// <reference path="~/Scripts/jquery-1.7.1-vsdoc.js" />

$(document).ready(function () {
    $("#product_right_ergo").hover(function () {
        $("#product_left_ergo").show();
        $("#product_left_base").hide();
        $("#product_left_channel").hide();
        $("#product_left_recessed").hide();
        $("#product_left_green").hide();
        $("#product_left_usa").hide();
        $("#product_left_tread").hide();
        $("#product_left_colors").hide();
    });

    $("#product_right_recessed").hover(function () {
        $("#product_left_ergo").hide();
        $("#product_left_base").hide();
        $("#product_left_channel").hide();
        $("#product_left_recessed").show();
        $("#product_left_green").hide();
        $("#product_left_usa").hide();
        $("#product_left_tread").hide();
        $("#product_left_colors").hide();
    });

    $("#product_right_base").hover(function () {
        $("#product_left_ergo").hide();
        $("#product_left_base").show();
        $("#product_left_channel").hide();
        $("#product_left_recessed").hide();
        $("#product_left_green").hide();
        $("#product_left_usa").hide();
        $("#product_left_tread").hide();
        $("#product_left_colors").hide();
    });

    $("#product_right_green").hover(function () {
        $("#product_left_ergo").hide();
        $("#product_left_base").hide();
        $("#product_left_channel").hide();
        $("#product_left_recessed").hide();
        $("#product_left_green").show();
        $("#product_left_usa").hide();
        $("#product_left_tread").hide();
        $("#product_left_colors").hide();
    });

    $("#product_right_channel").hover(function () {
        $("#product_left_ergo").hide();
        $("#product_left_base").hide();
        $("#product_left_channel").show();
        $("#product_left_recessed").hide();
        $("#product_left_green").hide();
        $("#product_left_usa").hide();
        $("#product_left_tread").hide();
        $("#product_left_colors").hide();
    });

    $("#product_right_usa").hover(function () {
        $("#product_left_ergo").hide();
        $("#product_left_base").hide();
        $("#product_left_channel").hide();
        $("#product_left_recessed").hide();
        $("#product_left_green").hide();
        $("#product_left_usa").show();
        $("#product_left_tread").hide();
        $("#product_left_colors").hide();
    });

    $("#product_right_tread").hover(function () {
        $("#product_left_ergo").hide();
        $("#product_left_base").hide();
        $("#product_left_channel").hide();
        $("#product_left_recessed").hide();
        $("#product_left_green").hide();
        $("#product_left_usa").hide();
        $("#product_left_tread").show();
        $("#product_left_colors").hide();
    });

    $("#product_right_colors").hover(function () {
        $("#product_left_ergo").hide();
        $("#product_left_base").hide();
        $("#product_left_channel").hide();
        $("#product_left_recessed").hide();
        $("#product_left_green").hide();
        $("#product_left_usa").hide();
        $("#product_left_tread").hide();
        $("#product_left_colors").show();
    });
});
