﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.NB;

namespace Nop.Data.Mapping.NB
{
	/// <summary>
	/// Represents a entity mapping configuration
	/// </summary>
	public partial class Sinek_Model_ViewMap : NopEntityTypeConfiguration<Sinek_Model_View>
    {
        #region Methods

        /// <summary>
        /// Configures the entity
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity</param>
        public override void Configure(EntityTypeBuilder<Sinek_Model_View> builder)
        {
            builder.ToTable("vw_Sinek_Model");
            builder.HasKey(m => m.Id);

            builder.Property(m => m.Name).HasMaxLength(400);
            builder.Property(m => m.Description);
            builder.Property(m => m.PageSize);
            builder.Property(m => m.PageSizeOptions).HasMaxLength(200);
            builder.Property(m => m.Published);
            builder.Property(m => m.Deleted);
            builder.Property(m => m.CreatedOnUtc);
            builder.Property(m => m.UpdatedOnUtc);
            builder.Property(m => m.ManufacturerId);
            builder.Property(m => m.ManufacturerName);
            builder.Property(m => m.DisplayOrder);

            base.Configure(builder);
        }

        #endregion
    }
}
