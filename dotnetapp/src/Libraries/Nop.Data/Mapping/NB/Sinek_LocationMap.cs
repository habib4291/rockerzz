﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.NB;

namespace Nop.Data.Mapping.NB
{
	/// <summary>
	/// Represents a entity mapping configuration
	/// </summary>
	public partial class Sinek_LocationMap : NopEntityTypeConfiguration<Sinek_Location>
	{
		#region Methods

		/// <summary>
		/// Configures the entity
		/// </summary>
		/// <param name="builder">The builder to be used to configure the entity</param>
		public override void Configure(EntityTypeBuilder<Sinek_Location> builder)
		{
			builder.ToTable("Sinek_Location");
			builder.HasKey(m => m.Id);

			builder.Property(m => m.Latitude);
			builder.Property(m => m.Longitude);
			builder.Property(m => m.LocationType);
			builder.Property(m => m.Name1);
			builder.Property(m => m.Name2);
			builder.Property(m => m.LocationAddress).IsRequired();
			builder.Property(m => m.PhoneNo1);
			builder.Property(m => m.PhoneNo2);
			builder.Property(m => m.Fax);
			builder.Property(m => m.WebSite);
			builder.Property(m => m.EMail);
			builder.Property(m => m.Notes);
			builder.Property(m => m.SellsSk8tape);
			builder.Property(m => m.SellsRockerz);

			builder.Property(m => m.PageSize);
			builder.Property(m => m.PageSizeOptions).HasMaxLength(200);
			builder.Property(m => m.Published);
			builder.Property(m => m.Deleted);
			builder.Property(m => m.CreatedOnUtc);
			builder.Property(m => m.UpdatedOnUtc);

			base.Configure(builder);
		}

		#endregion
	}
}
