﻿using Nop.Core;
using Nop.Core.Domain.NB;
using System.Collections.Generic;

namespace Nop.Services.NB
{
	/// <summary>
	/// Sinek_Model service
	/// </summary>
	public partial interface ISinek_Model_ViewService
    {
        /// <summary>
        /// Gets all sinek_Models
        /// </summary>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Sinek_Model collection</returns>
        IList<Sinek_Model_View> GetAllSinek_Models_View(bool showHidden = false);

        /// <summary>
        /// Gets all sinek_Models
        /// </summary>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Sinek_Models</returns>
        IPagedList<Sinek_Model_View> GetAllSinek_Models_View(int pageIndex, int pageSize, bool showHidden = false);

    }
}
