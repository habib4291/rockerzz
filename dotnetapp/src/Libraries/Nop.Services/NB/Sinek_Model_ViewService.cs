﻿using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Services.Events;
using Nop.Core.Domain.NB;

namespace Nop.Services.NB
{
	/// <summary>
	/// Sinek_Model service
	/// </summary>
	public partial class Sinek_Model_ViewService : ISinek_Model_ViewService
    {
        #region Constants
        private const string SINEK_MODEL_ALL_KEY = "Nop.sinek_Model.all-{0}";
        private const string SINEK_MODEL_BY_ID_KEY = "Nop.sinek_Model.id-{0}";
        private const string SINEK_MODEL_PATTERN_KEY = "Nop.sinek_Model.";
        #endregion

        #region Fields
        private readonly IRepository<Sinek_Model_View> _sinek_Model_ViewRepository;
        private readonly IRepository<Sinek_Model> _sinek_ModelRepository;
        private readonly IEventPublisher _eventPublisher;
        private readonly ICacheManager _cacheManager;
        #endregion

        #region Ctor

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="cacheManager">Cache manager</param>
        /// <param name="sinek_ModelRepository">Category repository</param>
        /// <param name="productSinek_ModelRepository">ProductCategory repository</param>
        /// <param name="productRepository">Product repository</param>
        /// <param name="eventPublisher">Event published</param>
        public Sinek_Model_ViewService(ICacheManager cacheManager,
            IRepository<Sinek_Model> sinek_ModelRepository,
            IRepository<Sinek_Model_View> sinek_Model_ViewRepository,
            IEventPublisher eventPublisher)
        {
            _cacheManager = cacheManager;
            _sinek_ModelRepository = sinek_ModelRepository;
            _sinek_Model_ViewRepository = sinek_Model_ViewRepository;
            _eventPublisher = eventPublisher;
        }
        #endregion

        #region Methods



        /// <summary>
        /// Gets all sinek_Models
        /// </summary>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Sinek_Model collection</returns>
        public virtual IList<Sinek_Model_View> GetAllSinek_Models_View(bool showHidden = false)
        {
            string key = string.Format(SINEK_MODEL_ALL_KEY, showHidden);


            return _cacheManager.Get(key, () =>
            {
                var query = from m in _sinek_Model_ViewRepository.Table
                            orderby m.Name
                            where (showHidden || m.Published) &&
                            !m.Deleted
                            select m;
                var sinek_Models = query.ToList();
                return sinek_Models;
            });
        }

        /// <summary>
        /// Gets all sinek_Models
        /// </summary>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Sinek_Models</returns>
        public virtual IPagedList<Sinek_Model_View> GetAllSinek_Models_View(int pageIndex, int pageSize, bool showHidden = false)
        {
            var sinek_Models = GetAllSinek_Models_View(showHidden);
            return new PagedList<Sinek_Model_View>(sinek_Models, pageIndex, pageSize);
        }
        #endregion
    }
}
