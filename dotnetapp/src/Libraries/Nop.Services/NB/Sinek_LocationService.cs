﻿using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Data;
using System.Data.SqlClient;
using System.Data;
using Nop.Services.Events;
using Nop.Core.Domain.NB;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Nop.Services.NB
{
	/// <summary>
	/// Sinek_Location service
	/// </summary>
	public partial class Sinek_LocationService : ISinek_LocationService
    {
        #region Constants
        private const string SINEK_LOCATION_ALL_KEY = "Nop.sinek_Location.all-{0}";
        private const string SINEK_LOCATION_BY_ID_KEY = "Nop.sinek_Location.id-{0}";
        private const string SINEK_LOCATION_PATTERN_KEY = "Nop.sinek_Location.";
        #endregion

        #region Fields

        private readonly IRepository<Sinek_Location> _sinek_LocationRepository;
        private readonly IEventPublisher _eventPublisher;
        private readonly ICacheManager _cacheManager;
        #endregion

        #region Ctor

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="cacheManager">Cache manager</param>
        /// <param name="sinek_LocationRepository">Category repository</param>
        /// <param name="productSinek_LocationRepository">ProductCategory repository</param>
        /// <param name="productRepository">Product repository</param>
        /// <param name="eventPublisher">Event published</param>
        public Sinek_LocationService(ICacheManager cacheManager,
            IRepository<Sinek_Location> sinek_LocationRepository,
            IEventPublisher eventPublisher)
        {
            _cacheManager = cacheManager;
            _sinek_LocationRepository = sinek_LocationRepository;
            _eventPublisher = eventPublisher;
        }
        #endregion

        #region Methods

        /// <summary>
        /// Deletes a sinek_Location
        /// </summary>
        /// <param name="sinek_Location">Sinek_Location</param>
        public virtual void DeleteSinek_Location(Sinek_Location sinek_Location)
        {
            if (sinek_Location == null)
                throw new ArgumentNullException("sinek_Location");

            sinek_Location.Deleted = true;
            UpdateSinek_Location(sinek_Location);
            //CreateXMLFile();
        }


        public void CreateXMLFile()
        {
            var conn = new SqlConnectionStringBuilder(DataSettingsManager.LoadSettings().DataConnectionString);
            string conString = conn.ToString();
            SqlConnection scCon = new SqlConnection();
            scCon.ConnectionString = conString;
            scCon.Open();

            SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM Sinek_Location where Deleted = 0 and Published = 1", scCon);
            DataSet ds = new DataSet();

            da.Fill(ds, "Employee");
            ds.WriteXml("~/App_Data/dataEmployee.xml");

            scCon.Close();

        }


        /// <summary>
        /// Gets all sinek_Locations
        /// </summary>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Sinek_Location collection</returns>
        public virtual IList<Sinek_Location> GetAllSinek_Locations(bool showHidden = false)
        {
            string key = string.Format(SINEK_LOCATION_ALL_KEY, showHidden);


            return _cacheManager.Get(key, () =>
            {
                var query = from m in _sinek_LocationRepository.Table
                            orderby m.Id
                            where (showHidden || m.Published) &&
                            !m.Deleted
                            select m;
                var sinek_Locations = query.ToList();
                return sinek_Locations;
            });
        }

        /// <summary>
        /// Gets all sinek_Locations
        /// </summary>
        /// <param name="name1">Name 1</param>
        /// <param name="name2">Name 2</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Sinek_Locations</returns>
        public virtual IPagedList<Sinek_Location> GetAllSinek_Locations(string name1, string name2, int pageIndex, int pageSize, bool showHidden = false)
        {
            var query = _sinek_LocationRepository.Table;
            if (!showHidden)
                query = query.Where(m => m.Published);

            if (!string.IsNullOrWhiteSpace(name1))
                query = query.Where(m => m.Name1.Contains(name1));

            if (!string.IsNullOrWhiteSpace(name2))
                query = query.Where(m => m.Name2.Contains(name2));

            query = query.Where(m => !m.Deleted);

            query = query.OrderByDescending(m => m.Id);

            return new PagedList<Sinek_Location>(query.ToList(), pageIndex, pageSize);
        }

        /// <summary>
        /// Gets a sinek_Location
        /// </summary>
        /// <param name="sinek_LocationId">Sinek_Location identifier</param>
        /// <returns>Sinek_Location</returns>
        public virtual Sinek_Location GetSinek_LocationById(int sinek_LocationId)
        {
            if (sinek_LocationId == 0)
                return null;

            string key = string.Format(SINEK_LOCATION_BY_ID_KEY, sinek_LocationId);
            return _cacheManager.Get(key, () =>
            {
                var sinek_Location = _sinek_LocationRepository.GetById(sinek_LocationId);
                return sinek_Location;
            });
        }

        /// <summary>
        /// Inserts a sinek_Location
        /// </summary>
        /// <param name="sinek_Location">Sinek_Location</param>
        public virtual void InsertSinek_Location(Sinek_Location sinek_Location)
        {
            if (sinek_Location == null)
                throw new ArgumentNullException("sinek_Location");


            _sinek_LocationRepository.Insert(sinek_Location);

            //CreateXMLFile();
            //cache
            _cacheManager.RemoveByPrefix(SINEK_LOCATION_PATTERN_KEY);
            //_cacheManager.RemoveByPrefix(PRODUCTSINEK_LOCATION_PATTERN_KEY);

            //event notification
            _eventPublisher.EntityInserted(sinek_Location);


        }

        /// <summary>
        /// Updates the sinek_Location
        /// </summary>
        /// <param name="sinek_Location">Sinek_Location</param>
        public virtual void UpdateSinek_Location(Sinek_Location sinek_Location)
        {
            if (sinek_Location == null)
                throw new ArgumentNullException("sinek_Location");

            _sinek_LocationRepository.Update(sinek_Location);
            //CreateXMLFile();
            //cache
            _cacheManager.RemoveByPrefix(SINEK_LOCATION_PATTERN_KEY);
            //_cacheManager.RemoveByPrefix(PRODUCTSINEK_LOCATION_PATTERN_KEY);

            //event notification
            _eventPublisher.EntityUpdated(sinek_Location);
        }
        ///// <summary>
        ///// Deletes sinek_Locations 
        ///// </summary>
        ///// <param name="location">location</param>
        public virtual void DeleteLocation(Sinek_Location location)
        {
            if (location == null)
                throw new ArgumentNullException("location");
            //Delete it from memory
            var loc = (from s1 in _sinek_LocationRepository.Table
                       where s1.Id == location.Id
                       select s1).FirstOrDefault();
            _sinek_LocationRepository.Delete(loc);
            UpdateSinek_Location(loc);
        }

        /// <summary>
        /// Get locations by identifiers
        /// </summary>
        /// <param name="locationIds">Sinek_Location identifiers</param>
        /// <returns>locations</returns>
        public virtual IList<Sinek_Location> GetLocationsByIds(int[] locationIds)
        {
            if (locationIds == null || locationIds.Length == 0)
                return new List<Sinek_Location>();

            var query = from l in _sinek_LocationRepository.Table
                        where locationIds.Contains(l.Id)
                        select l;
            var locations = query.ToList();
            //sort by passed identifiers
            var sortedLocations = new List<Sinek_Location>();
            foreach (int id in locationIds)
            {
                var location = locations.Find(x => x.Id == id);
                if (location != null)
                    sortedLocations.Add(location);
            }
            return sortedLocations;
        }

        #endregion
    }
}
