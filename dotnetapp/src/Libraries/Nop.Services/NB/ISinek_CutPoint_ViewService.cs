﻿using Nop.Core;
using Nop.Core.Domain.NB;
using System.Collections.Generic;

namespace Nop.Services.NB
{
	/// <summary>
	/// Sinek_CutPoint service
	/// </summary>
	public partial interface ISinek_CutPoint_ViewService
    {
        /// <summary>
        /// Gets all sinek_CutPoints
        /// </summary>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Sinek_CutPoint collection</returns>
        IList<Sinek_CutPoint_View> GetAllSinek_CutPoints_View(bool showHidden = false);

        /// <summary>
        /// Gets all sinek_CutPoints
        /// </summary>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Sinek_CutPoints</returns>
        IPagedList<Sinek_CutPoint_View> GetAllSinek_CutPoints_View(int pageIndex, int pageSize, bool showHidden = false);

    }
}
