﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.NB;
using Nop.Services.Events;

namespace Nop.Services.NB
{
	/// <summary>
	/// Sinek_Model service
	/// </summary>
	public partial class Sinek_ModelService : ISinek_ModelService
	{
		#region Constants

		private const string SINEK_MODEL_ALL_KEY = "Nop.sinek_Model.all-{0}";
		private const string SINEK_MODEL_BY_ID_KEY = "Nop.sinek_Model.id-{0}";
		private const string SINEK_MODEL_PATTERN_KEY = "Nop.sinek_Model.";
		public static int ProductVariantAttributeIdForSinek_Model = 20;

		#endregion

		#region Fields

		private readonly IRepository<Sinek_Model> _sinek_ModelRepository;
		private readonly IRepository<Sinek_Model_View> _sinek_Model_ViewRepository;
		private readonly IRepository<ProductAttributeValue> _productAttributeValueRepository;
		private readonly IEventPublisher _eventPublisher;
		private readonly ICacheManager _cacheManager;

		#endregion

		#region Ctor

		/// <summary>
		/// Ctor
		/// </summary>
		/// <param name="cacheManager">Cache manager</param>
		/// <param name="sinek_ModelRepository">Category repository</param>
		/// <param name="productSinek_ModelRepository">ProductCategory repository</param>
		/// <param name="productRepository">Product repository</param>
		/// <param name="eventPublisher">Event published</param>
		public Sinek_ModelService(ICacheManager cacheManager,
			IRepository<Sinek_Model> sinek_ModelRepository,
			IRepository<Sinek_Model_View> sinek_Model_ViewRepository,
			IRepository<ProductAttributeValue> productAttributeValueRepository,
			IEventPublisher eventPublisher)
		{
			this._cacheManager = cacheManager;
			this._sinek_ModelRepository = sinek_ModelRepository;
			this._sinek_Model_ViewRepository = sinek_Model_ViewRepository;
			this._productAttributeValueRepository = productAttributeValueRepository;
			this._eventPublisher = eventPublisher;
		}
		#endregion

		#region Methods

		/// <summary>
		/// Deletes a sinek_Model
		/// </summary>
		/// <param name="sinek_Model">Sinek_Model</param>
		public virtual void DeleteSinek_Model(Sinek_Model sinek_Model)
		{
			if (sinek_Model == null)
				throw new ArgumentNullException("sinek_Model");

			sinek_Model.Deleted = true;
			UpdateSinek_Model(sinek_Model);
		}

		public virtual void DeleteSinek_Model_ProductVariantAttributeValue(Sinek_Model sinek_Model)
		{
			if (sinek_Model == null)
				throw new ArgumentNullException("sinek_Model");

			#region Here we handle ProductVariantAttributeValue
			//delete ProductAttributeValue
			ProductAttributeValue objProductAttributeValue = _productAttributeValueRepository.Table.Where(x => x.ProductAttributeMappingId == ProductVariantAttributeIdForSinek_Model && x.Sinek_ModelId == sinek_Model.Id).FirstOrDefault();
			if (objProductAttributeValue != null)
			{
				_productAttributeValueRepository.Delete(objProductAttributeValue);
				_eventPublisher.EntityDeleted(objProductAttributeValue);
			}

			//Core.Data.DataSettingsManager dataSettingsManager = new DataSettingsManager();
			//string conString = dataSettingsManager.LoadSettings().DataConnectionString;
			//SqlConnection scCon = new SqlConnection();
			//scCon.ConnectionString = conString;
			//scCon.Open();

			//string sqlCommand = "DELETE FROM ProductVariantAttributeValue " +
			//                    "WHERE ProductVariantAttributeId = " + ProductVariantAttributeIdForSinek_Model.ToString() + " AND Sinek_ModelId = " + sinek_Model.Id.ToString() + " ";

			//SqlCommand scCom = new SqlCommand(sqlCommand, scCon);
			//scCom.ExecuteNonQuery();
			//scCon.Close();

			#endregion
		}

		/// <summary>
		/// Gets all sinek_Models
		/// </summary>
		/// <param name="showHidden">A value indicating whether to show hidden records</param>
		/// <returns>Sinek_Model collection</returns>
		public virtual IList<Sinek_Model> GetAllSinek_Models(bool showHidden = false)
		{
			string key = string.Format(SINEK_MODEL_ALL_KEY, showHidden);


			return _cacheManager.Get(key, () =>
			{
				var query = from m in _sinek_ModelRepository.Table
							orderby m.Name
							where (showHidden || m.Published) &&
							!m.Deleted
							select m;
				var sinek_Models = query.ToList();
				return sinek_Models;
			});
		}

		/// <summary>
		/// Gets all sinek_Models
		/// </summary>
		/// <param name="showHidden">A value indicating whether to show hidden records</param>
		/// <returns>Sinek_Model collection</returns>
		public virtual IList<Sinek_Model_View> GetAllSinek_Models_View(bool showHidden = false)
		{
			string key = string.Format(SINEK_MODEL_ALL_KEY, showHidden);


			return _cacheManager.Get(key, () =>
			{
				var query = from m in _sinek_Model_ViewRepository.Table
							orderby m.Name
							where (showHidden || m.Published) &&
							!m.Deleted
							select m;
				query = query.OrderBy(x => x.DisplayOrder).ThenBy(x => x.Id);
				var sinek_Models = query.ToList();
				return sinek_Models;
			});
		}

		/// <summary>
		/// Gets all sinek_Models
		/// </summary>
		/// <param name="pageIndex">Page index</param>
		/// <param name="pageSize">Page size</param>
		/// <param name="showHidden">A value indicating whether to show hidden records</param>
		/// <returns>Sinek_Models</returns>
		public virtual IPagedList<Sinek_Model_View> GetAllSinek_Models_View(string modelName, int manufactureId, int pageIndex, int pageSize, bool showHidden = false)
		{
			var query = _sinek_Model_ViewRepository.Table;
			if (!showHidden)
				query = query.Where(m => m.Published);
			if (!string.IsNullOrWhiteSpace(modelName))
				query = query.Where(m => m.Name.Contains(modelName));
			if (manufactureId > 0)
				query = query.Where(m => m.ManufacturerId == manufactureId);
			query = query.Where(m => !m.Deleted);
			query = query.OrderBy(m => m.DisplayOrder).ThenBy(c => c.Id);

			return new PagedList<Sinek_Model_View>(query.ToList(), pageIndex, pageSize);
		}

		/// <summary>
		/// Gets all sinek_Models
		/// </summary>
		/// <param name="pageIndex">Page index</param>
		/// <param name="pageSize">Page size</param>
		/// <param name="showHidden">A value indicating whether to show hidden records</param>
		/// <returns>Sinek_Models</returns>
		public virtual IPagedList<Sinek_Model> GetAllSinek_Models(int pageIndex, int pageSize, bool showHidden = false)
		{
			var sinek_Models = GetAllSinek_Models(showHidden);
			return new PagedList<Sinek_Model>(sinek_Models, pageIndex, pageSize);
		}

		/// <summary>
		/// Gets a sinek_Model
		/// </summary>
		/// <param name="sinek_ModelId">Sinek_Model identifier</param>
		/// <returns>Sinek_Model</returns>
		public virtual Sinek_Model GetSinek_ModelById(int sinek_ModelId)
		{
			if (sinek_ModelId == 0)
				return null;

			string key = string.Format(SINEK_MODEL_BY_ID_KEY, sinek_ModelId);
			return _cacheManager.Get(key, () =>
			{
				var sinek_Model = _sinek_ModelRepository.GetById(sinek_ModelId);
				return sinek_Model;
			});
		}

		/// <summary>
		/// Inserts a sinek_Model
		/// </summary>
		/// <param name="sinek_Model">Sinek_Model</param>
		public virtual void InsertSinek_Model(Sinek_Model sinek_Model)
		{
			if (sinek_Model == null)
				throw new ArgumentNullException("sinek_Model");

			_sinek_ModelRepository.Insert(sinek_Model);

			//cache
			_cacheManager.RemoveByPrefix(SINEK_MODEL_PATTERN_KEY);
			//_cacheManager.RemoveByPattern(PRODUCTSINEK_MODEL_PATTERN_KEY);

			//event notification
			_eventPublisher.EntityInserted(sinek_Model);

			#region Here we handle ProductVariantAttributeValue
			ProductAttributeValue objProductAttributeValue = new ProductAttributeValue();
			objProductAttributeValue.ProductAttributeMappingId = ProductVariantAttributeIdForSinek_Model;
			objProductAttributeValue.Name = sinek_Model.Name;
			objProductAttributeValue.ManufacturerId = 0;
			objProductAttributeValue.PriceAdjustment = 0;
			objProductAttributeValue.WeightAdjustment = 0;
			objProductAttributeValue.IsPreSelected = false;
			objProductAttributeValue.DisplayOrder = 0;
			objProductAttributeValue.Sinek_CutPointId = 0;
			objProductAttributeValue.Sinek_ModelId = sinek_Model.Id;
			_productAttributeValueRepository.Insert(objProductAttributeValue);
			_eventPublisher.EntityInserted(objProductAttributeValue);

			//Core.Data.DataSettingsManager dataSettingsManager = new DataSettingsManager();
			//string conString = dataSettingsManager.LoadSettings().DataConnectionString;
			//SqlConnection scCon = new SqlConnection();
			//scCon.ConnectionString = conString;
			//scCon.Open();

			//string sqlCommand = "INSERT INTO ProductVariantAttributeValue " +
			//                    "(ProductVariantAttributeId,Name,Sinek_ModelId,PriceAdjustment,WeightAdjustment,IsPreSelected,DisplayOrder,ManufacturerId,Sinek_CutPointId) " +
			//                    "VALUES(" + ProductVariantAttributeIdForSinek_Model.ToString() + ",'" + sinek_Model.Name.ToString() + "', " + sinek_Model.Id + ",0,0,0,0,0,0) ";

			//SqlCommand scCom = new SqlCommand(sqlCommand, scCon);
			//scCom.ExecuteNonQuery();
			//scCon.Close();

			#endregion
		}

		/// <summary>
		/// Updates the sinek_Model
		/// </summary>
		/// <param name="sinek_Model">Sinek_Model</param>
		public virtual void UpdateSinek_Model(Sinek_Model sinek_Model)
		{
			if (sinek_Model == null)
				throw new ArgumentNullException("sinek_Model");

			_sinek_ModelRepository.Update(sinek_Model);

			//cache
			_cacheManager.RemoveByPrefix(SINEK_MODEL_PATTERN_KEY);
			//_cacheManager.RemoveByPattern(PRODUCTSINEK_MODEL_PATTERN_KEY);

			//event notification
			_eventPublisher.EntityUpdated(sinek_Model);

			#region Here we handle ProductVariantAttributeValue

			ProductAttributeValue objProductAttributeValue = _productAttributeValueRepository.Table.Where(x => x.ProductAttributeMappingId == ProductVariantAttributeIdForSinek_Model && x.Sinek_ModelId == sinek_Model.Id).FirstOrDefault();
			objProductAttributeValue.ProductAttributeMappingId = ProductVariantAttributeIdForSinek_Model;
			objProductAttributeValue.Name = sinek_Model.Name;
			objProductAttributeValue.ManufacturerId = 0;
			objProductAttributeValue.PriceAdjustment = 0;
			objProductAttributeValue.WeightAdjustment = 0;
			objProductAttributeValue.IsPreSelected = false;
			objProductAttributeValue.DisplayOrder = 0;
			objProductAttributeValue.Sinek_CutPointId = 0;
			objProductAttributeValue.Sinek_ModelId = sinek_Model.Id;
			_productAttributeValueRepository.Update(objProductAttributeValue);
			_eventPublisher.EntityUpdated(objProductAttributeValue);

			//Core.Data.DataSettingsManager dataSettingsManager = new DataSettingsManager();
			//string conString = dataSettingsManager.LoadSettings().DataConnectionString;
			//SqlConnection scCon = new SqlConnection();
			//scCon.ConnectionString = conString;
			//scCon.Open();

			//string sqlCommand = "Update ProductVariantAttributeValue set Name =  '" + sinek_Model.Name.ToString() + "' " +
			//                    "WHERE ProductVariantAttributeId = " + ProductVariantAttributeIdForSinek_Model.ToString() + " AND Sinek_ModelId = " + sinek_Model.Id.ToString() + " ";

			//SqlCommand scCom = new SqlCommand(sqlCommand, scCon);
			//scCom.ExecuteNonQuery();
			//scCon.Close();

			#endregion

		}




		public virtual IList<Sinek_Model> GetSinek_ModelByManufacturerId(int manufacturerId, bool showHidden = false)
		{
			string key = string.Format(SINEK_MODEL_ALL_KEY, manufacturerId);
			return _cacheManager.Get(key, () =>
			{
				var query = from sp in _sinek_ModelRepository.Table
							orderby sp.Name
							where sp.ManufacturerId == manufacturerId &&
							(showHidden || sp.Published)
							&&
							!sp.Deleted
							select sp;
				var sinek_Model = query.ToList();
				return sinek_Model;
			});
		}

		public class Sinek_ModelInfo
		{
			public virtual int Id { get; set; }
			public virtual string Name { get; set; }
			public virtual string Description { get; set; }
			public virtual int PageSize { get; set; }
			public virtual string PageSizeOptions { get; set; }
			public virtual bool Published { get; set; }
			public virtual bool Deleted { get; set; }
			public virtual DateTime CreatedOnUtc { get; set; }
			public virtual DateTime UpdatedOnUtc { get; set; }
			public virtual int ManufacturerId { get; set; }
			public virtual int DisplayOrder { get; set; }
		}

		#endregion
	}
}
