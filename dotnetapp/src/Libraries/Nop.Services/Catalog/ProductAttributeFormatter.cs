using System;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Messages;
using Nop.Core.Domain.Orders;
using Nop.Core.Html;
using Nop.Services.Configuration;
using Nop.Services.Directory;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Messages;
using Nop.Services.NB;
using Nop.Services.Tax;

namespace Nop.Services.Catalog
{
	/// <summary>
	/// Product attribute formatter
	/// </summary>
	public partial class ProductAttributeFormatter : IProductAttributeFormatter
	{
		#region Fields

		private readonly ICurrencyService _currencyService;
		private readonly IDownloadService _downloadService;
		private readonly ILocalizationService _localizationService;
		private readonly IPriceCalculationService _priceCalculationService;
		private readonly IPriceFormatter _priceFormatter;
		private readonly IProductAttributeParser _productAttributeParser;
		private readonly ITaxService _taxService;
		private readonly IWebHelper _webHelper;
		private readonly IWorkContext _workContext;
		private readonly ShoppingCartSettings _shoppingCartSettings;
		private readonly IProductAttributeService _productAttributeService;
		private readonly IStoreContext _storeContext;
		private readonly IEmailAccountService _emailAccountService;
		private readonly EmailAccountSettings _emailAccountSettings;
		private readonly ISettingService _settingService;
		private readonly IQueuedEmailService _queuedEmailService;
		private readonly ILogger _logger;
		private readonly ISinek_CutPointService _sinek_CutPointService;

		#endregion

		#region Ctor

		public ProductAttributeFormatter(ICurrencyService currencyService,
			IDownloadService downloadService,
			ILocalizationService localizationService,
			IPriceCalculationService priceCalculationService,
			IPriceFormatter priceFormatter,
			IProductAttributeParser productAttributeParser,
			ITaxService taxService,
			IWebHelper webHelper,
			IWorkContext workContext,
			ShoppingCartSettings shoppingCartSettings,
			IProductAttributeService productAttributeService,
			IStoreContext storeContext,
			IEmailAccountService emailAccountService,
			EmailAccountSettings emailAccountSettings,
			ISettingService settingService,
			IQueuedEmailService queuedEmailService,
			ILogger logger,
			ISinek_CutPointService sinek_CutPointService)
		{
			_currencyService = currencyService;
			_downloadService = downloadService;
			_localizationService = localizationService;
			_priceCalculationService = priceCalculationService;
			_priceFormatter = priceFormatter;
			_productAttributeParser = productAttributeParser;
			_taxService = taxService;
			_webHelper = webHelper;
			_workContext = workContext;
			_shoppingCartSettings = shoppingCartSettings;
			_productAttributeService = productAttributeService;
			_storeContext = storeContext;
			_emailAccountService = emailAccountService;
			_emailAccountSettings = emailAccountSettings;
			_settingService = settingService;
			_queuedEmailService = queuedEmailService;
			_logger = logger;
			_sinek_CutPointService = sinek_CutPointService;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Formats attributes
		/// </summary>
		/// <param name="product">Product</param>
		/// <param name="attributesXml">Attributes in XML format</param>
		/// <returns>Attributes</returns>
		public virtual string FormatAttributes(Product product, string attributesXml)
		{
			var customer = _workContext.CurrentCustomer;
			return FormatAttributes(product, attributesXml, customer);
		}

		/// <summary>
		/// Formats attributes
		/// </summary>
		/// <param name="product">Product</param>
		/// <param name="attributesXml">Attributes in XML format</param>
		/// <param name="customer">Customer</param>
		/// <param name="separator">Separator</param>
		/// <param name="htmlEncode">A value indicating whether to encode (HTML) values</param>
		/// <param name="renderPrices">A value indicating whether to render prices</param>
		/// <param name="renderProductAttributes">A value indicating whether to render product attributes</param>
		/// <param name="renderGiftCardAttributes">A value indicating whether to render gift card attributes</param>
		/// <param name="allowHyperlinks">A value indicating whether to HTML hyperink tags could be rendered (if required)</param>
		/// <returns>Attributes</returns>
		public virtual string FormatAttributes(Product product, string attributesXml,
			Customer customer, string separator = "<br />", bool htmlEncode = true, bool renderPrices = true,
			bool renderProductAttributes = true, bool renderGiftCardAttributes = true,
			bool allowHyperlinks = true)
		{
			var result = new StringBuilder();

			string serapator = "<br />";
			if (_storeContext.CurrentStore.Id == 1)
			{
				var strGuardFirst = "<b>Guard 1: </b> ";
				var strGuardSecond = "<b>Guard 2: </b> ";
				var strFrontLeft = string.Empty;
				var strFrontRight = string.Empty;
				var strBackLeft = string.Empty;
				var strBackRight = string.Empty;
				var showGuard = true;

				//attributes
				if (renderProductAttributes)
				{
					var attributes = _productAttributeParser.ParseProductAttributeMappings(attributesXml);
					for (int i = 0; i < attributes.Count; i++)
					{
						var attribute = attributes[i];
						if (attribute.TextPrompt == "Guard Colors" || attribute.TextPrompt == "Towel Color")
							continue;
						var valuesStr = _productAttributeParser.ParseValues(attributesXml, attribute.Id);
						for (int j = 0; j < valuesStr.Count; j++)
						{
							string valueStr = valuesStr[j];
							string formattedAttribute = string.Empty;
							if (!attribute.ShouldHaveValues())
							{
								//no values
								if (attribute.AttributeControlType == AttributeControlType.MultilineTextbox)
								{
									//multiline textbox
									var attributeName = _localizationService.GetLocalized(attribute.ProductAttribute, a => a.Name, _workContext.WorkingLanguage.Id);
									//encode (if required)
									if (htmlEncode)
										attributeName = HttpUtility.HtmlEncode(attributeName);
									formattedAttribute = string.Format("{0}: {1}", attributeName, HtmlHelper.FormatText(valueStr, false, true, false, false, false, false));
									//we never encode multiline textbox input
								}
								else if (attribute.AttributeControlType == AttributeControlType.FileUpload)
								{
									//file upload
									Guid downloadGuid;
									Guid.TryParse(valueStr, out downloadGuid);
									var download = _downloadService.GetDownloadByGuid(downloadGuid);
									if (download != null)
									{
										//TODO add a method for getting URL (use routing because it handles all SEO friendly URLs)
										string attributeText = "";
										var fileName = string.Format("{0}{1}",
											download.Filename ?? download.DownloadGuid.ToString(),
											download.Extension);
										//encode (if required)
										if (htmlEncode)
											fileName = HttpUtility.HtmlEncode(fileName);
										if (allowHyperlinks)
										{
											//hyperlinks are allowed
											var downloadLink = string.Format("{0}download/getfileupload/?downloadId={1}", _webHelper.GetStoreLocation(false), download.DownloadGuid);
											attributeText = string.Format("<a href=\"{0}\" class=\"fileuploadattribute\">{1}</a>", downloadLink, fileName);
										}
										else
										{
											//hyperlinks aren't allowed
											attributeText = fileName;
										}
										var attributeName = _localizationService.GetLocalized(attribute.ProductAttribute, a => a.Name, _workContext.WorkingLanguage.Id);
										//encode (if required)
										if (htmlEncode)
											attributeName = HttpUtility.HtmlEncode(attributeName);
										formattedAttribute = string.Format("{0}: {1}", attributeName, attributeText);
									}
								}
								else
								{
									//other attributes (textbox, datepicker)
									formattedAttribute = string.Format("{0}: {1}", _localizationService.GetLocalized(attribute.ProductAttribute, a => a.Name, _workContext.WorkingLanguage.Id), valueStr);
									//encode (if required)
									if (htmlEncode)
										formattedAttribute = HttpUtility.HtmlEncode(formattedAttribute);
								}
							}
							else
							{
								//attributes with values
								int attributeValueId;
								if (int.TryParse(valueStr, out attributeValueId))
								{
									var attributeValue = _productAttributeService.GetProductAttributeValueById(attributeValueId);
									if (attributeValue != null)
									{
										formattedAttribute = string.Format("{0}: {1}", _localizationService.GetLocalized(attribute.ProductAttribute, a => a.Name, _workContext.WorkingLanguage.Id), _localizationService.GetLocalized(attributeValue, a => a.Name, _workContext.WorkingLanguage.Id));
										var title = string.Format("{0}", _localizationService.GetLocalized(attribute.ProductAttribute, a => a.Name, _workContext.WorkingLanguage.Id), _localizationService.GetLocalized(attributeValue, a => a.Name, _workContext.WorkingLanguage.Id));
										var getAttributeValue = string.Format("{1}", _localizationService.GetLocalized(attribute.ProductAttribute, a => a.Name, _workContext.WorkingLanguage.Id), _localizationService.GetLocalized(attributeValue, a => a.Name, _workContext.WorkingLanguage.Id));
										formattedAttribute = "<b>" + title + ": </b>" + attributeValue.Name;

										if (title == "Blade Cut Point") { formattedAttribute = string.Empty; }

										if (title == "Front Left" || title == "Front Right" || title == "Back Left" || title == "Back Right")
										{
											formattedAttribute = string.Empty;
											showGuard = false;

											if (title == "Front Left")
											{
												strFrontLeft = getAttributeValue;
											}
											else if (title == "Front Right")
											{
												strFrontRight = getAttributeValue;
											}

											else if (title == "Back Left")
											{
												strBackLeft = getAttributeValue;
											}
											else if (title == "Back Right")
											{
												strBackRight = getAttributeValue;
											}

											if (!String.IsNullOrEmpty(strFrontLeft) && !String.IsNullOrEmpty(strFrontRight))
											{
												strGuardFirst += strFrontLeft + " / " + strFrontRight;
											}

											else if (!String.IsNullOrEmpty(strBackLeft) && !String.IsNullOrEmpty(strBackRight))
											{
												strGuardSecond += strBackLeft + " / " + strBackRight;
											}
										}

										if (renderPrices)
										{
											decimal taxRate;
											decimal attributeValuePriceAdjustment = _priceCalculationService.GetProductAttributeValuePriceAdjustment(attributeValue, customer);
											decimal priceAdjustmentBase = _taxService.GetProductPrice(product, attributeValuePriceAdjustment, customer, out taxRate);
											decimal priceAdjustment = _currencyService.ConvertFromPrimaryStoreCurrency(priceAdjustmentBase, _workContext.WorkingCurrency);
											if (priceAdjustmentBase > 0)
											{
												string priceAdjustmentStr = _priceFormatter.FormatPrice(priceAdjustment, false, false);
												formattedAttribute += string.Format(" [+{0}]", priceAdjustmentStr);
											}
											else if (priceAdjustmentBase < decimal.Zero)
											{
												string priceAdjustmentStr = _priceFormatter.FormatPrice(-priceAdjustment, false, false);
												formattedAttribute += string.Format(" [-{0}]", priceAdjustmentStr);
											}
										}

										//display quantity
										if (_shoppingCartSettings.RenderAssociatedAttributeValueQuantity &&
											attributeValue.AttributeValueType == AttributeValueType.AssociatedToProduct)
										{
											//render only when more than 1
											if (attributeValue.Quantity > 1)
											{
												//TODO localize resource
												formattedAttribute += string.Format(" - qty {0}", attributeValue.Quantity);
											}
										}
									}

									//encode (if required)
									if (htmlEncode)
										//pvaAttribute = HttpUtility.HtmlEncode(pvaAttribute);

										if (!String.IsNullOrEmpty(strFrontLeft) && !String.IsNullOrEmpty(strFrontRight))
										{
											formattedAttribute = strGuardFirst;
											strFrontLeft = strFrontRight = string.Empty;
										}

										else if (!String.IsNullOrEmpty(strBackLeft) && !String.IsNullOrEmpty(strBackRight))
										{
											formattedAttribute = strGuardSecond;
											strBackLeft = strBackRight = string.Empty;
										}

									formattedAttribute = HttpUtility.HtmlDecode(formattedAttribute);
								}
							}

							if (!String.IsNullOrEmpty(formattedAttribute))
							{
								if (i != 0 || j != 0)
									result.Append(serapator);
								result.Append(formattedAttribute);
							}
						}
					}
				}
			}
			else
			{
				//attributes
				if (renderProductAttributes)
				{
					foreach (var attribute in _productAttributeParser.ParseProductAttributeMappings(attributesXml))
					{
						//attributes without values
						if (!attribute.ShouldHaveValues())
						{
							foreach (var value in _productAttributeParser.ParseValues(attributesXml, attribute.Id))
							{
								var formattedAttribute = string.Empty;
								if (attribute.AttributeControlType == AttributeControlType.MultilineTextbox)
								{
									//multiline textbox
									var attributeName = _localizationService.GetLocalized(attribute.ProductAttribute, a => a.Name, _workContext.WorkingLanguage.Id);

									//encode (if required)
									if (htmlEncode)
										attributeName = WebUtility.HtmlEncode(attributeName);

									//we never encode multiline textbox input
									formattedAttribute = $"{attributeName}: {HtmlHelper.FormatText(value, false, true, false, false, false, false)}";
								}
								else if (attribute.AttributeControlType == AttributeControlType.FileUpload)
								{
									//file upload
									Guid.TryParse(value, out var downloadGuid);
									var download = _downloadService.GetDownloadByGuid(downloadGuid);
									if (download != null)
									{
										var fileName = $"{download.Filename ?? download.DownloadGuid.ToString()}{download.Extension}";

										//encode (if required)
										if (htmlEncode)
											fileName = WebUtility.HtmlEncode(fileName);

										//TODO add a method for getting URL (use routing because it handles all SEO friendly URLs)
										var attributeText = allowHyperlinks ? $"<a href=\"{_webHelper.GetStoreLocation(false)}download/getfileupload/?downloadId={download.DownloadGuid}\" class=\"fileuploadattribute\">{fileName}</a>"
											: fileName;

										var attributeName = _localizationService.GetLocalized(attribute.ProductAttribute, a => a.Name, _workContext.WorkingLanguage.Id);

										//encode (if required)
										if (htmlEncode)
											attributeName = WebUtility.HtmlEncode(attributeName);

										formattedAttribute = $"{attributeName}: {attributeText}";
									}
								}
								else
								{
									//other attributes (textbox, datepicker)
									formattedAttribute = $"{_localizationService.GetLocalized(attribute.ProductAttribute, a => a.Name, _workContext.WorkingLanguage.Id)}: {value}";

									//encode (if required)
									if (htmlEncode)
										formattedAttribute = WebUtility.HtmlEncode(formattedAttribute);
								}

								if (string.IsNullOrEmpty(formattedAttribute))
									continue;

								if (result.Length > 0)
									result.Append(separator);
								result.Append(formattedAttribute);
							}
						}
						//product attribute values
						else
						{
							foreach (var attributeValue in _productAttributeParser.ParseProductAttributeValues(attributesXml, attribute.Id))
							{
								var formattedAttribute = $"{_localizationService.GetLocalized(attribute.ProductAttribute, a => a.Name, _workContext.WorkingLanguage.Id)}: {_localizationService.GetLocalized(attributeValue, a => a.Name, _workContext.WorkingLanguage.Id)}";

								if (renderPrices)
								{
									if (attributeValue.PriceAdjustmentUsePercentage)
									{
										if (attributeValue.PriceAdjustment > decimal.Zero)
										{
											formattedAttribute += string.Format(
													_localizationService.GetResource("FormattedAttributes.PriceAdjustment"),
													"+", attributeValue.PriceAdjustment.ToString("G29"), "%");
										}
										else if (attributeValue.PriceAdjustment < decimal.Zero)
										{
											formattedAttribute += string.Format(
													_localizationService.GetResource("FormattedAttributes.PriceAdjustment"),
													string.Empty, attributeValue.PriceAdjustment.ToString("G29"), "%");
										}
									}
									else
									{
										var attributeValuePriceAdjustment = _priceCalculationService.GetProductAttributeValuePriceAdjustment(attributeValue, customer);
										var priceAdjustmentBase = _taxService.GetProductPrice(product, attributeValuePriceAdjustment, customer, out var _);
										var priceAdjustment = _currencyService.ConvertFromPrimaryStoreCurrency(priceAdjustmentBase, _workContext.WorkingCurrency);

										if (priceAdjustmentBase > decimal.Zero)
										{
											formattedAttribute += string.Format(
													_localizationService.GetResource("FormattedAttributes.PriceAdjustment"),
													"+", _priceFormatter.FormatPrice(priceAdjustment, false, false), string.Empty);
										}
										else if (priceAdjustmentBase < decimal.Zero)
										{
											formattedAttribute += string.Format(
													_localizationService.GetResource("FormattedAttributes.PriceAdjustment"),
													"-", _priceFormatter.FormatPrice(-priceAdjustment, false, false), string.Empty);
										}
									}
								}

								//display quantity
								if (_shoppingCartSettings.RenderAssociatedAttributeValueQuantity && attributeValue.AttributeValueType == AttributeValueType.AssociatedToProduct)
								{
									//render only when more than 1
									if (attributeValue.Quantity > 1)
										formattedAttribute += string.Format(_localizationService.GetResource("ProductAttributes.Quantity"), attributeValue.Quantity);
								}

								//encode (if required)
								if (htmlEncode)
									formattedAttribute = WebUtility.HtmlEncode(formattedAttribute);

								if (string.IsNullOrEmpty(formattedAttribute))
									continue;

								if (result.Length > 0)
									result.Append(separator);
								result.Append(formattedAttribute);
							}
						}
					}
				}
			}

			//gift cards
			if (!renderGiftCardAttributes)
				return result.ToString();

			if (!product.IsGiftCard)
				return result.ToString();

			_productAttributeParser.GetGiftCardAttribute(attributesXml, out var giftCardRecipientName, out var giftCardRecipientEmail, out var giftCardSenderName, out var giftCardSenderEmail, out var _);

			//sender
			var giftCardFrom = product.GiftCardType == GiftCardType.Virtual ?
				string.Format(_localizationService.GetResource("GiftCardAttribute.From.Virtual"), giftCardSenderName, giftCardSenderEmail) :
				string.Format(_localizationService.GetResource("GiftCardAttribute.From.Physical"), giftCardSenderName);
			//recipient
			var giftCardFor = product.GiftCardType == GiftCardType.Virtual ?
				string.Format(_localizationService.GetResource("GiftCardAttribute.For.Virtual"), giftCardRecipientName, giftCardRecipientEmail) :
				string.Format(_localizationService.GetResource("GiftCardAttribute.For.Physical"), giftCardRecipientName);

			//encode (if required)
			if (htmlEncode)
			{
				giftCardFrom = WebUtility.HtmlEncode(giftCardFrom);
				giftCardFor = WebUtility.HtmlEncode(giftCardFor);
			}

			if (!string.IsNullOrEmpty(result.ToString()))
			{
				result.Append(separator);
			}

			result.Append(giftCardFrom);
			result.Append(separator);
			result.Append(giftCardFor);

			return result.ToString();
		}

		#region Custom

		// Flag true means it is requested from admin section
		/// <summary>
		/// Formats attributes 2
		/// </summary>
		/// <param name="product">Product</param>
		/// <param name="attributesXml">Attributes in XML format</param>
		/// <param name="customer">Customer</param>
		/// <param name="serapator">Serapator</param>
		/// <param name="htmlEncode">A value indicating whether to encode (HTML) values</param>
		/// <param name="renderPrices">A value indicating whether to render prices</param>
		/// <param name="renderProductAttributes">A value indicating whether to render product attributes</param>
		/// <param name="renderGiftCardAttributes">A value indicating whether to render gift card attributes</param>
		/// <param name="allowHyperlinks">A value indicating whether to HTML hyperink tags could be rendered (if required)</param>
		/// <returns>Attributes</returns>
		public string FormatAttributes2(Product product, string attributesXml,
			Customer customer, string serapator = "<br />", bool htmlEncode = true, bool renderPrices = true,
			bool renderProductAttributes = true, bool renderGiftCardAttributes = true,
			bool allowHyperlinks = true)
		{
			var result = new StringBuilder();
			var strGuardFirst = "<b>Guard 1: </b> ";
			var strGuardSecond = "<b>Guard 2: </b> ";
			var strFrontLeft = string.Empty;
			var strFrontRight = string.Empty;
			var strBackLeft = string.Empty;
			var strBackRight = string.Empty;
			var showGuard = true;

			//attributes
			if (renderProductAttributes)
			{
				var pvaCollection = _productAttributeParser.ParseProductAttributeMappings(attributesXml);
				for (int i = 0; i < pvaCollection.Count; i++)
				{
					var pva = pvaCollection[i];

					var valuesStr = _productAttributeParser.ParseValues(attributesXml, pva.Id);
					for (int j = 0; j < valuesStr.Count; j++)
					{
						string valueStr = valuesStr[j];
						string pvaAttribute = string.Empty;
						if (!pva.ShouldHaveValues())
						{
							//no values
							if (pva.AttributeControlType == AttributeControlType.MultilineTextbox)
							{
								//multiline textbox
								var attributeName = _localizationService.GetLocalized(pva.ProductAttribute, a => a.Name, _workContext.WorkingLanguage.Id);
								//encode (if required)
								if (htmlEncode)
									attributeName = HttpUtility.HtmlEncode(attributeName);
								pvaAttribute = string.Format("{0}: {1}", attributeName, HtmlHelper.FormatText(valueStr, false, true, false, false, false, false));
								//we never encode multiline textbox input
							}
							else if (pva.AttributeControlType == AttributeControlType.FileUpload)
							{
								//file upload
								var download = _downloadService.GetDownloadByGuid(Guid.Parse(valueStr));
								if (download != null)
								{
									//TODO add a method for getting URL (use routing because it handles all SEO friendly URLs)
									string attributeText = "";
									var fileName = string.Format("{0}{1}",
										download.Filename ?? download.DownloadGuid.ToString(),
										download.Extension);
									//encode (if required)
									if (htmlEncode)
										fileName = HttpUtility.HtmlEncode(fileName);
									if (allowHyperlinks)
									{
										//hyperlinks are allowed
										var downloadLink = string.Format("{0}download/getfileupload/?downloadId={1}", _webHelper.GetStoreLocation(false), download.DownloadGuid);
										attributeText = string.Format("<a href=\"{0}\" class=\"fileuploadattribute\">{1}</a>", downloadLink, fileName);
									}
									else
									{
										//hyperlinks aren't allowed
										attributeText = fileName;
									}
									var attributeName = _localizationService.GetLocalized(pva.ProductAttribute, a => a.Name, _workContext.WorkingLanguage.Id);
									//encode (if required)
									if (htmlEncode)
										attributeName = HttpUtility.HtmlEncode(attributeName);
									pvaAttribute = string.Format("{0}: {1}", attributeName, attributeText);
								}
							}
							else
							{
								//other attributes (textbox, datepicker)
								pvaAttribute = string.Format("{0}: {1}", _localizationService.GetLocalized(pva.ProductAttribute, a => a.Name, _workContext.WorkingLanguage.Id), valueStr);
								//encode (if required)
								if (htmlEncode)
									pvaAttribute = HttpUtility.HtmlEncode(pvaAttribute);
							}
						}
						else
						{
							//attributes with values
							int pvaId = 0;
							if (int.TryParse(valueStr, out pvaId))
							{
								var pvaValue = _productAttributeService.GetProductAttributeValueById(pvaId);
								if (pvaValue != null)
								{
									pvaAttribute = string.Format("{0}: {1}", _localizationService.GetLocalized(pva.ProductAttribute, a => a.Name, _workContext.WorkingLanguage.Id), _localizationService.GetLocalized(pvaValue, a => a.Name, _workContext.WorkingLanguage.Id));
									var title = string.Format("{0}", _localizationService.GetLocalized(pva.ProductAttribute, a => a.Name, _workContext.WorkingLanguage.Id), _localizationService.GetLocalized(pvaValue, a => a.Name, _workContext.WorkingLanguage.Id));
									var attributeValue = string.Format("{1}", _localizationService.GetLocalized(pva.ProductAttribute, a => a.Name, _workContext.WorkingLanguage.Id), _localizationService.GetLocalized(pvaValue, a => a.Name, _workContext.WorkingLanguage.Id));
									pvaAttribute = "<b>" + title + ": </b>" + attributeValue;

									if (title == "Blade Cut Point") { pvaAttribute = string.Empty; }

									if (title == "Front Left" || title == "Front Right" || title == "Back Left" || title == "Back Right")
									{
										pvaAttribute = string.Empty;
										showGuard = false;

										if (title == "Front Left")
										{
											strFrontLeft = attributeValue;
										}
										else if (title == "Front Right")
										{
											strFrontRight = attributeValue;
										}

										else if (title == "Back Left")
										{
											strBackLeft = attributeValue;
										}
										else if (title == "Back Right")
										{
											strBackRight = attributeValue;
										}

										if (!String.IsNullOrEmpty(strFrontLeft) && !String.IsNullOrEmpty(strFrontRight))
										{
											strGuardFirst += strFrontLeft + " / " + strFrontRight;
										}

										else if (!String.IsNullOrEmpty(strBackLeft) && !String.IsNullOrEmpty(strBackRight))
										{
											strGuardSecond += strBackLeft + " / " + strBackRight;
										}
									}

									if (renderPrices)
									{
										decimal taxRate = decimal.Zero;
										decimal priceAdjustmentBase = _taxService.GetProductPrice(product, pvaValue.PriceAdjustment, customer, out taxRate);
										decimal priceAdjustment = _currencyService.ConvertFromPrimaryStoreCurrency(priceAdjustmentBase, _workContext.WorkingCurrency);
										if (priceAdjustmentBase > 0)
										{
											string priceAdjustmentStr = _priceFormatter.FormatPrice(priceAdjustment, false, false);
											pvaAttribute += string.Format(" [+{0}]", priceAdjustmentStr);
										}
										else if (priceAdjustmentBase < decimal.Zero)
										{
											string priceAdjustmentStr = _priceFormatter.FormatPrice(-priceAdjustment, false, false);
											pvaAttribute += string.Format(" [-{0}]", priceAdjustmentStr);
										}
									}
								}
								//encode (if required)
								if (htmlEncode)
									//pvaAttribute = HttpUtility.HtmlEncode(pvaAttribute);

									if (!String.IsNullOrEmpty(strFrontLeft) && !String.IsNullOrEmpty(strFrontRight))
									{
										pvaAttribute = strGuardFirst;
										strFrontLeft = strFrontRight = string.Empty;
									}

									else if (!String.IsNullOrEmpty(strBackLeft) && !String.IsNullOrEmpty(strBackRight))
									{
										pvaAttribute = strGuardSecond;
										strBackLeft = strBackRight = string.Empty;
									}

								pvaAttribute = HttpUtility.HtmlDecode(pvaAttribute);
							}
						}

						if (!String.IsNullOrEmpty(pvaAttribute))
						{
							if (i != 0 || j != 0)
								result.Append(serapator);
							result.Append(pvaAttribute);
						}
					}
				}
			}

			//gift cards
			if (renderGiftCardAttributes)
			{
				if (product.IsGiftCard)
				{
					string giftCardRecipientName = "";
					string giftCardRecipientEmail = "";
					string giftCardSenderName = "";
					string giftCardSenderEmail = "";
					string giftCardMessage = "";
					_productAttributeParser.GetGiftCardAttribute(attributesXml, out giftCardRecipientName, out giftCardRecipientEmail,
						out giftCardSenderName, out giftCardSenderEmail, out giftCardMessage);

					if (!String.IsNullOrEmpty(result.ToString()))
					{
						result.Append(serapator);
					}
					//encode (if required)
					if (htmlEncode)
					{
						giftCardRecipientName = HttpUtility.HtmlEncode(giftCardRecipientName);
						giftCardSenderName = HttpUtility.HtmlEncode(giftCardSenderName);
					}

					result.Append(string.Format(_localizationService.GetResource("GiftCardAttribute.For"), giftCardRecipientName));
					result.Append(serapator);
					result.Append(string.Format(_localizationService.GetResource("GiftCardAttribute.From"), giftCardSenderName));
				}
			}
			return result.ToString();
		}


		/// <summary>
		/// Formats attributes for Cut Point
		/// </summary>
		/// <param name="product">Product</param>
		/// <param name="attributesXml">Attributes in XML format</param>
		/// <param name="customer">Customer</param>
		/// <param name="serapator">Serapator</param>
		/// <param name="htmlEncode">A value indicating whether to encode (HTML) values</param>
		/// <param name="renderPrices">A value indicating whether to render prices</param>
		/// <param name="renderProductAttributes">A value indicating whether to render product attributes</param>
		/// <param name="renderGiftCardAttributes">A value indicating whether to render gift card attributes</param>
		/// <param name="allowHyperlinks">A value indicating whether to HTML hyperink tags could be rendered (if required)</param>
		/// <returns>Attributes</returns>
		public string FormatAttributesCutPoint(Product product, string attributesXml,
		   Customer customer, string serapator = "<br />", bool htmlEncode = true, bool renderPrices = true,
		   bool renderProductAttributes = true, bool renderGiftCardAttributes = true,
		   bool allowHyperlinks = true)
		{
			var result = new StringBuilder();

			if (_storeContext.CurrentStore.Id == 1)
			{
				var strGuardFirst = "<b>Guard 1: </b> ";
				var strGuardSecond = "<b>Guard 2: </b> ";
				var strFrontLeft = string.Empty;
				var strFrontRight = string.Empty;
				var strBackLeft = string.Empty;
				var strBackRight = string.Empty;
				var showGuard = true;

				//cut point
				int _manufacturerId = 0;
				int _modelId = 0;
				string _size = "";

				//attributes
				if (renderProductAttributes)
				{
					var pvaCollection = _productAttributeParser.ParseProductAttributeMappings(attributesXml);
					for (int i = 0; i < pvaCollection.Count; i++)
					{
						var pva = pvaCollection[i];
						if (pva.TextPrompt == "Guard Colors" || pva.TextPrompt == "Towel Color")
							continue;
						var valuesStr = _productAttributeParser.ParseValues(attributesXml, pva.Id);
						for (int j = 0; j < valuesStr.Count; j++)
						{
							string valueStr = valuesStr[j];
							string pvaAttribute = string.Empty;
							if (!pva.ShouldHaveValues())
							{
								//no values
								if (pva.AttributeControlType == AttributeControlType.MultilineTextbox)
								{
									//multiline textbox
									var attributeName = _localizationService.GetLocalized(pva.ProductAttribute, a => a.Name, _workContext.WorkingLanguage.Id);
									//encode (if required)
									if (htmlEncode)
										attributeName = HttpUtility.HtmlEncode(attributeName);
									pvaAttribute = string.Format("{0}: {1}", attributeName, HtmlHelper.FormatText(valueStr, false, true, false, false, false, false));
									//we never encode multiline textbox input
								}
								else if (pva.AttributeControlType == AttributeControlType.FileUpload)
								{
									//file upload
									var download = _downloadService.GetDownloadByGuid(Guid.Parse(valueStr));
									if (download != null)
									{
										//TODO add a method for getting URL (use routing because it handles all SEO friendly URLs)
										string attributeText = "";
										var fileName = string.Format("{0}{1}",
											download.Filename ?? download.DownloadGuid.ToString(),
											download.Extension);
										//encode (if required)
										if (htmlEncode)
											fileName = HttpUtility.HtmlEncode(fileName);
										if (allowHyperlinks)
										{
											//hyperlinks are allowed
											var downloadLink = string.Format("{0}download/getfileupload/?downloadId={1}", _webHelper.GetStoreLocation(false), download.DownloadGuid);
											attributeText = string.Format("<a href=\"{0}\" class=\"fileuploadattribute\">{1}</a>", downloadLink, fileName);
										}
										else
										{
											//hyperlinks aren't allowed
											attributeText = fileName;
										}
										var attributeName = _localizationService.GetLocalized(pva.ProductAttribute, a => a.Name, _workContext.WorkingLanguage.Id);
										//encode (if required)
										if (htmlEncode)
											attributeName = HttpUtility.HtmlEncode(attributeName);
										pvaAttribute = string.Format("{0}: {1}", attributeName, attributeText);
									}
								}
								else
								{
									//other attributes (textbox, datepicker)
									pvaAttribute = string.Format("{0}: {1}", _localizationService.GetLocalized(pva.ProductAttribute, a => a.Name, _workContext.WorkingLanguage.Id), valueStr);
									//encode (if required)
									if (htmlEncode)
										pvaAttribute = HttpUtility.HtmlEncode(pvaAttribute);
								}
							}
							else
							{
								//attributes with values
								int pvaId = 0;
								if (int.TryParse(valueStr, out pvaId))
								{
									var pvaValue = _productAttributeService.GetProductAttributeValueById(pvaId);
									if (pvaValue != null)
									{
										pvaAttribute = string.Format("{0}: {1}", _localizationService.GetLocalized(pva.ProductAttribute, a => a.Name, _workContext.WorkingLanguage.Id), _localizationService.GetLocalized(pvaValue, a => a.Name, _workContext.WorkingLanguage.Id));
										var title = string.Format("{0}", _localizationService.GetLocalized(pva.ProductAttribute, a => a.Name, _workContext.WorkingLanguage.Id), _localizationService.GetLocalized(pvaValue, a => a.Name, _workContext.WorkingLanguage.Id));
										var attributeValue = string.Format("{1}", _localizationService.GetLocalized(pva.ProductAttribute, a => a.Name, _workContext.WorkingLanguage.Id), _localizationService.GetLocalized(pvaValue, a => a.Name, _workContext.WorkingLanguage.Id));
										pvaAttribute = "<b>" + title + ": </b>" + attributeValue;

										if (title == "Blade Make")
										{
											_manufacturerId = pvaValue.ManufacturerId;
										}
										if (title == "Blade Model")
										{
											_modelId = pvaValue.Sinek_ModelId;
										}
										if (title == "Blade Size")
										{
											_size = pvaValue.Name;
										}


										if (title == "Blade Cut Point") { pvaAttribute = string.Empty; }

										if (title == "Front Left" || title == "Front Right" || title == "Back Left" || title == "Back Right")
										{
											pvaAttribute = string.Empty;
											showGuard = false;

											if (title == "Front Left")
											{
												strFrontLeft = attributeValue;
											}
											else if (title == "Front Right")
											{
												strFrontRight = attributeValue;
											}

											else if (title == "Back Left")
											{
												strBackLeft = attributeValue;
											}
											else if (title == "Back Right")
											{
												strBackRight = attributeValue;
											}

											if (!String.IsNullOrEmpty(strFrontLeft) && !String.IsNullOrEmpty(strFrontRight))
											{
												strGuardFirst += strFrontLeft + " / " + strFrontRight;
											}

											else if (!String.IsNullOrEmpty(strBackLeft) && !String.IsNullOrEmpty(strBackRight))
											{
												strGuardSecond += strBackLeft + " / " + strBackRight;
											}
										}

										if (renderPrices)
										{
											decimal taxRate = decimal.Zero;
											decimal priceAdjustmentBase = _taxService.GetProductPrice(product, pvaValue.PriceAdjustment, customer, out taxRate);
											decimal priceAdjustment = _currencyService.ConvertFromPrimaryStoreCurrency(priceAdjustmentBase, _workContext.WorkingCurrency);
											if (priceAdjustmentBase > 0)
											{
												string priceAdjustmentStr = _priceFormatter.FormatPrice(priceAdjustment, false, false);
												pvaAttribute += string.Format(" [+{0}]", priceAdjustmentStr);
											}
											else if (priceAdjustmentBase < decimal.Zero)
											{
												string priceAdjustmentStr = _priceFormatter.FormatPrice(-priceAdjustment, false, false);
												pvaAttribute += string.Format(" [-{0}]", priceAdjustmentStr);
											}
										}
									}
									//encode (if required)
									if (htmlEncode)
										//pvaAttribute = HttpUtility.HtmlEncode(pvaAttribute);

										if (!String.IsNullOrEmpty(strFrontLeft) && !String.IsNullOrEmpty(strFrontRight))
										{
											pvaAttribute = strGuardFirst;
											strFrontLeft = strFrontRight = string.Empty;
										}

										else if (!String.IsNullOrEmpty(strBackLeft) && !String.IsNullOrEmpty(strBackRight))
										{
											pvaAttribute = strGuardSecond;
											strBackLeft = strBackRight = string.Empty;
										}

									pvaAttribute = HttpUtility.HtmlDecode(pvaAttribute);
								}
							}

							if (!String.IsNullOrEmpty(pvaAttribute))
							{
								if (i != 0 || j != 0)
									result.Append(serapator);
								result.Append(pvaAttribute);
							}
						}
					}
				}

				if (_manufacturerId == 0 || _modelId == 0)
				{
					var emailAccount = _emailAccountService.GetEmailAccountById(_emailAccountSettings.DefaultEmailAccountId);
					if (emailAccount == null)
						emailAccount = _emailAccountService.GetAllEmailAccounts().FirstOrDefault();
					if (emailAccount == null)
						throw new Exception("No email account could be loaded");

					string body = string.Empty;
					if (_manufacturerId == 0)
						body = _localizationService.GetResource("ManufacturerId.Missing");
					else if (_modelId == 0)
						body = _localizationService.GetResource("ModelId.Missing");
					else
						body = _localizationService.GetResource("ManufacturerAndModelId.Missing");

					string cc = string.Empty;
					if (!String.IsNullOrEmpty(_settingService.GetSettingByKey<string>("Manufacturer_Model_Missing_CC")))
					{
						cc = _settingService.GetSettingByKey<string>("Manufacturer_Model_Missing_CC");
					}

					_queuedEmailService.InsertQueuedEmail(new QueuedEmail
					{
						From = emailAccount.Email,
						FromName = emailAccount.DisplayName,
						To = emailAccount.Email,
						ToName = emailAccount.DisplayName,
						CC = cc,
						Priority = QueuedEmailPriority.High,
						Subject = _localizationService.GetResource("RockerzSkateGuards.Subject"),
						Body = body,
						CreatedOnUtc = DateTime.UtcNow,
						EmailAccountId = emailAccount.Id
					});
				}

				// Add cut point string
				string log = String.Format("Parameters Values {0}, {1}, {2}", _manufacturerId, _modelId, _size);
				_logger.Information(log);
				if (_manufacturerId != 0 && _modelId != 0 && _size != "")
				{
					try
					{
						_logger.Information("Before Function Starts");
						var _cutpoint = _sinek_CutPointService.GetSinek_CutPointByManufacturerIdModelIdSize(_modelId, _manufacturerId, _size);
						_logger.Information("Cut Point Count : " + _cutpoint.Count);
						if (_cutpoint.Count > 0)
						{
							_logger.Information("Only CutPoint : " + _cutpoint[0].CutPoint);
							string str = "<b>Cut Point: </b>" + _cutpoint[0].CutPoint;

							if (!String.IsNullOrEmpty(_cutpoint[0].AdditionalInfo))
							{
								str += " " + _cutpoint[0].AdditionalInfo;
								_logger.Information("CutPoint and AdditionalInfo : " + _cutpoint[0].CutPoint + " " + _cutpoint[0].AdditionalInfo);
							}

							result.Append(serapator);
							result.Append(str);
						}
						_logger.Information("After Function Ends");
					}
					catch (Exception exe)
					{
						_logger.Error("Error Getting CutPoint", exe);
					}
				}
			}
			else
			{
				//attributes
				if (renderProductAttributes)
				{
					var attributes = _productAttributeParser.ParseProductAttributeMappings(attributesXml);
					for (int i = 0; i < attributes.Count; i++)
					{
						var attribute = attributes[i];
						var valuesStr = _productAttributeParser.ParseValues(attributesXml, attribute.Id);
						for (int j = 0; j < valuesStr.Count; j++)
						{
							string valueStr = valuesStr[j];
							string formattedAttribute = string.Empty;
							if (!attribute.ShouldHaveValues())
							{
								//no values
								if (attribute.AttributeControlType == AttributeControlType.MultilineTextbox)
								{
									//multiline textbox
									var attributeName = _localizationService.GetLocalized(attribute.ProductAttribute, a => a.Name, _workContext.WorkingLanguage.Id);
									//encode (if required)
									if (htmlEncode)
										attributeName = HttpUtility.HtmlEncode(attributeName);
									formattedAttribute = string.Format("{0}: {1}", attributeName, HtmlHelper.FormatText(valueStr, false, true, false, false, false, false));
									//we never encode multiline textbox input
								}
								else if (attribute.AttributeControlType == AttributeControlType.FileUpload)
								{
									//file upload
									Guid downloadGuid;
									Guid.TryParse(valueStr, out downloadGuid);
									var download = _downloadService.GetDownloadByGuid(downloadGuid);
									if (download != null)
									{
										//TODO add a method for getting URL (use routing because it handles all SEO friendly URLs)
										string attributeText = "";
										var fileName = string.Format("{0}{1}",
											download.Filename ?? download.DownloadGuid.ToString(),
											download.Extension);
										//encode (if required)
										if (htmlEncode)
											fileName = HttpUtility.HtmlEncode(fileName);
										if (allowHyperlinks)
										{
											//hyperlinks are allowed
											var downloadLink = string.Format("{0}download/getfileupload/?downloadId={1}", _webHelper.GetStoreLocation(false), download.DownloadGuid);
											attributeText = string.Format("<a href=\"{0}\" class=\"fileuploadattribute\">{1}</a>", downloadLink, fileName);
										}
										else
										{
											//hyperlinks aren't allowed
											attributeText = fileName;
										}
										var attributeName = _localizationService.GetLocalized(attribute.ProductAttribute, a => a.Name, _workContext.WorkingLanguage.Id);
										//encode (if required)
										if (htmlEncode)
											attributeName = HttpUtility.HtmlEncode(attributeName);
										formattedAttribute = string.Format("{0}: {1}", attributeName, attributeText);
									}
								}
								else
								{
									//other attributes (textbox, datepicker)
									formattedAttribute = string.Format("{0}: {1}", _localizationService.GetLocalized(attribute.ProductAttribute, a => a.Name, _workContext.WorkingLanguage.Id), valueStr);
									//encode (if required)
									if (htmlEncode)
										formattedAttribute = HttpUtility.HtmlEncode(formattedAttribute);
								}
							}
							else
							{
								//attributes with values
								int attributeValueId;
								if (int.TryParse(valueStr, out attributeValueId))
								{
									var attributeValue = _productAttributeService.GetProductAttributeValueById(attributeValueId);
									if (attributeValue != null)
									{
										formattedAttribute = string.Format("{0}: {1}", _localizationService.GetLocalized(attribute.ProductAttribute, a => a.Name, _workContext.WorkingLanguage.Id), _localizationService.GetLocalized(attributeValue, a => a.Name, _workContext.WorkingLanguage.Id));

										if (renderPrices)
										{
											decimal taxRate;
											decimal attributeValuePriceAdjustment = _priceCalculationService.GetProductAttributeValuePriceAdjustment(attributeValue, customer);
											decimal priceAdjustmentBase = _taxService.GetProductPrice(product, attributeValuePriceAdjustment, customer, out taxRate);
											decimal priceAdjustment = _currencyService.ConvertFromPrimaryStoreCurrency(priceAdjustmentBase, _workContext.WorkingCurrency);
											if (priceAdjustmentBase > 0)
											{
												string priceAdjustmentStr = _priceFormatter.FormatPrice(priceAdjustment, false, false);
												formattedAttribute += string.Format(" [+{0}]", priceAdjustmentStr);
											}
											else if (priceAdjustmentBase < decimal.Zero)
											{
												string priceAdjustmentStr = _priceFormatter.FormatPrice(-priceAdjustment, false, false);
												formattedAttribute += string.Format(" [-{0}]", priceAdjustmentStr);
											}
										}

										//display quantity
										if (_shoppingCartSettings.RenderAssociatedAttributeValueQuantity &&
											attributeValue.AttributeValueType == AttributeValueType.AssociatedToProduct)
										{
											//render only when more than 1
											if (attributeValue.Quantity > 1)
											{
												//TODO localize resource
												formattedAttribute += string.Format(" - qty {0}", attributeValue.Quantity);
											}
										}
									}
									//encode (if required)
									if (htmlEncode)
										formattedAttribute = HttpUtility.HtmlEncode(formattedAttribute);
								}
							}

							if (!String.IsNullOrEmpty(formattedAttribute))
							{
								if (i != 0 || j != 0)
									result.Append(serapator);
								result.Append(formattedAttribute);
							}
						}
					}
				}
			}

			//gift cards
			if (renderGiftCardAttributes)
			{
				if (product.IsGiftCard)
				{
					string giftCardRecipientName = "";
					string giftCardRecipientEmail = "";
					string giftCardSenderName = "";
					string giftCardSenderEmail = "";
					string giftCardMessage = "";
					_productAttributeParser.GetGiftCardAttribute(attributesXml, out giftCardRecipientName, out giftCardRecipientEmail,
						out giftCardSenderName, out giftCardSenderEmail, out giftCardMessage);

					if (!String.IsNullOrEmpty(result.ToString()))
					{
						result.Append(serapator);
					}
					//encode (if required)
					if (htmlEncode)
					{
						giftCardRecipientName = HttpUtility.HtmlEncode(giftCardRecipientName);
						giftCardSenderName = HttpUtility.HtmlEncode(giftCardSenderName);
					}

					result.Append(string.Format(_localizationService.GetResource("GiftCardAttribute.For"), giftCardRecipientName));
					result.Append(serapator);
					result.Append(string.Format(_localizationService.GetResource("GiftCardAttribute.From"), giftCardSenderName));
				}
			}

			return result.ToString();
		}


		/// <summary>
		/// Formats Attributes For Title
		/// </summary>
		/// <param name="product">Product</param>
		/// <param name="attributesXml">Attributes in XML format</param>
		/// <returns>Attributes</returns>
		public string FormatAttributesForTitle(Product product, string attributesXml)
		{
			var customer = _workContext.CurrentCustomer;
			return FormatAttributesForTitle(product, attributesXml, customer);
		}


		/// <summary>
		/// Formats attributes for Title
		/// </summary>
		/// <param name="product">Product</param>
		/// <param name="attributesXml">Attributes in XML format</param>
		/// <param name="customer">Customer</param>
		/// <param name="serapator">Serapator</param>
		/// <param name="htmlEncode">A value indicating whether to encode (HTML) values</param>
		/// <param name="renderPrices">A value indicating whether to render prices</param>
		/// <param name="renderProductAttributes">A value indicating whether to render product attributes</param>
		/// <param name="renderGiftCardAttributes">A value indicating whether to render gift card attributes</param>
		/// <param name="allowHyperlinks">A value indicating whether to HTML hyperink tags could be rendered (if required)</param>
		/// <returns>Attributes</returns>
		public string FormatAttributesForTitle(Product product, string attributesXml,
		 Customer customer, string serapator = "<br />", bool htmlEncode = true, bool renderPrices = true,
		 bool renderProductAttributes = true, bool renderGiftCardAttributes = true,
		 bool allowHyperlinks = true)
		{
			var result = new StringBuilder();

			//attributes
			if (renderProductAttributes)
			{
				var pvaCollection = _productAttributeParser.ParseProductAttributeMappings(attributesXml);
				//ProductVariantAttribute itempva = null;
				//foreach (var item in pvaCollection)
				//{
				//    if (item.TextPrompt == "Guard Colors")
				//    {
				//        itempva = item;
				//        break;
				//    }
				//}
				//if (itempva != null)
				//    pvaCollection.Remove(itempva);

				for (int i = 0; i < pvaCollection.Count; i++)
				{
					var pva = pvaCollection[i];
					var valuesStr = _productAttributeParser.ParseValues(attributesXml, pva.Id);
					for (int j = 0; j < valuesStr.Count; j++)
					{
						string valueStr = valuesStr[j];
						string pvaAttribute = string.Empty;
						if (!pva.ShouldHaveValues())
						{
							//no values
							if (pva.AttributeControlType == AttributeControlType.MultilineTextbox)
							{
								//multiline textbox
								var attributeName = _localizationService.GetLocalized(pva.ProductAttribute, a => a.Name, _workContext.WorkingLanguage.Id);
								//encode (if required)
								if (htmlEncode)
									attributeName = HttpUtility.HtmlEncode(attributeName);
								pvaAttribute = string.Format("{0}: {1}", attributeName, HtmlHelper.FormatText(valueStr, false, true, false, false, false, false));
								//we never encode multiline textbox input
							}
							else if (pva.AttributeControlType == AttributeControlType.FileUpload)
							{
								//file upload
								var download = _downloadService.GetDownloadByGuid(Guid.Parse(valueStr));
								if (download != null)
								{
									//TODO add a method for getting URL (use routing because it handles all SEO friendly URLs)
									string attributeText = "";
									var fileName = string.Format("{0}{1}",
										download.Filename ?? download.DownloadGuid.ToString(),
										download.Extension);
									//encode (if required)
									if (htmlEncode)
										fileName = HttpUtility.HtmlEncode(fileName);
									if (allowHyperlinks)
									{
										//hyperlinks are allowed
										var downloadLink = string.Format("{0}download/getfileupload/?downloadId={1}", _webHelper.GetStoreLocation(false), download.DownloadGuid);
										attributeText = string.Format("<a href=\"{0}\" class=\"fileuploadattribute\">{1}</a>", downloadLink, fileName);
									}
									else
									{
										//hyperlinks aren't allowed
										attributeText = fileName;
									}
									var attributeName = _localizationService.GetLocalized(pva.ProductAttribute, a => a.Name, _workContext.WorkingLanguage.Id);
									//encode (if required)
									if (htmlEncode)
										attributeName = HttpUtility.HtmlEncode(attributeName);
									pvaAttribute = string.Format("{0}: {1}", attributeName, attributeText);
								}
							}
							else
							{
								//other attributes (textbox, datepicker)
								pvaAttribute = string.Format("{0}: {1}", _localizationService.GetLocalized(pva.ProductAttribute, a => a.Name, _workContext.WorkingLanguage.Id), valueStr);
								//encode (if required)
								if (htmlEncode)
									pvaAttribute = HttpUtility.HtmlEncode(pvaAttribute);
							}
						}
						else
						{
							//attributes with values
							int pvaId = 0;
							if (int.TryParse(valueStr, out pvaId))
							{
								var pvaValue = _productAttributeService.GetProductAttributeValueById(pvaId);
								if (pvaValue != null)
								{
									pvaAttribute = string.Format("{0}: {1}", _localizationService.GetLocalized(pva.ProductAttribute, a => a.Name, _workContext.WorkingLanguage.Id), _localizationService.GetLocalized(pvaValue, a => a.Name, _workContext.WorkingLanguage.Id));
									var title = string.Format("{0}", _localizationService.GetLocalized(pva.ProductAttribute, a => a.Name, _workContext.WorkingLanguage.Id), _localizationService.GetLocalized(pvaValue, a => a.Name, _workContext.WorkingLanguage.Id));
									var attributeValue = string.Format("{1}", _localizationService.GetLocalized(pva.ProductAttribute, a => a.Name, _workContext.WorkingLanguage.Id), _localizationService.GetLocalized(pvaValue, a => a.Name, _workContext.WorkingLanguage.Id));
									//var value3 = _localizationService.GetLocalized(pva.ProductAttribute, a => a.Name, _workContext.WorkingLanguage.Id);
									//var value4 = _localizationService.GetLocalized(pvaValue, a => a.Name, _workContext.WorkingLanguage.Id);
									//pvaAttribute = "<b>" + title + ": </b>" + attributeValue;
									if (renderPrices)
									{
										decimal taxRate = decimal.Zero;
										decimal priceAdjustmentBase = _taxService.GetProductPrice(product, pvaValue.PriceAdjustment, customer, out taxRate);
										decimal priceAdjustment = _currencyService.ConvertFromPrimaryStoreCurrency(priceAdjustmentBase, _workContext.WorkingCurrency);
										if (priceAdjustmentBase > 0)
										{
											string priceAdjustmentStr = _priceFormatter.FormatPrice(priceAdjustment, false, false);
											pvaAttribute += string.Format(" [+{0}]", priceAdjustmentStr);
										}
										else if (priceAdjustmentBase < decimal.Zero)
										{
											string priceAdjustmentStr = _priceFormatter.FormatPrice(-priceAdjustment, false, false);
											pvaAttribute += string.Format(" [-{0}]", priceAdjustmentStr);
										}
									}
								}
								//encode (if required)
								if (htmlEncode)
									pvaAttribute = HttpUtility.HtmlEncode(pvaAttribute);
								//pvaAttribute = HttpUtility.HtmlDecode(pvaAttribute);
							}
						}

						if (!String.IsNullOrEmpty(pvaAttribute))
						{
							if (i != 0 || j != 0)
								result.Append(serapator);
							result.Append(pvaAttribute);
						}
					}
				}
			}

			//gift cards
			if (renderGiftCardAttributes)
			{
				if (product.IsGiftCard)
				{
					string giftCardRecipientName = "";
					string giftCardRecipientEmail = "";
					string giftCardSenderName = "";
					string giftCardSenderEmail = "";
					string giftCardMessage = "";
					_productAttributeParser.GetGiftCardAttribute(attributesXml, out giftCardRecipientName, out giftCardRecipientEmail,
						out giftCardSenderName, out giftCardSenderEmail, out giftCardMessage);

					if (!String.IsNullOrEmpty(result.ToString()))
					{
						result.Append(serapator);
					}
					//encode (if required)
					if (htmlEncode)
					{
						giftCardRecipientName = HttpUtility.HtmlEncode(giftCardRecipientName);
						giftCardSenderName = HttpUtility.HtmlEncode(giftCardSenderName);
					}

					result.Append(string.Format(_localizationService.GetResource("GiftCardAttribute.For"), giftCardRecipientName));
					result.Append(serapator);
					result.Append(string.Format(_localizationService.GetResource("GiftCardAttribute.From"), giftCardSenderName));
				}
			}
			return result.ToString();
		}

		#endregion

		#endregion
	}
}