﻿namespace Nop.Core.Domain.NB
{
    /// <summary>
    /// Represents an address type enumeration
    /// </summary>
    public enum AddressType
    {
        /// <summary>
        /// Home
        /// </summary>
        Home = 10,

        /// <summary>
        /// Office
        /// </summary>
        Office = 20,

        /// <summary>
        /// Other
        /// </summary>
        Other = 30,
    }
}
