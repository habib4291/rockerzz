﻿using Nop.Core.Domain.Localization;
using System;

namespace Nop.Core.Domain.NB
{
	/// <summary>
	/// Represents a Sinek_CutPoint
	/// </summary>
	public partial class Sinek_CutPoint : BaseEntity, ILocalizedEntity
    {
        public int ManufacturerId { get; set; }
        public int ModelId { get; set; }
        public string Size { get; set; }
        public string CutPoint { get; set; }
        public string AdditionalInfo { get; set; }
        public int PageSize { get; set; }
        public string PageSizeOptions { get; set; }
        public bool Published { get; set; }
        public bool Deleted { get; set; }
        public DateTime CreatedOnUtc { get; set; }
        public DateTime UpdatedOnUtc { get; set; }
        public int DisplayOrder { get; set; }
    }
}
