﻿using Nop.Core.Domain.Localization;
using System;

namespace Nop.Core.Domain.NB
{
	/// <summary>
	/// Represents a Sinek_Model_View
	/// </summary>
	public partial class Sinek_Model_View : BaseEntity, ILocalizedEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int PageSize { get; set; }
        public string PageSizeOptions { get; set; }
        public bool Published { get; set; }
        public bool Deleted { get; set; }
        public DateTime CreatedOnUtc { get; set; }
        public DateTime UpdatedOnUtc { get; set; }
        public int ManufacturerId { get; set; }
        public string ManufacturerName { get; set; }
        public int DisplayOrder { get; set; }
    }
}
